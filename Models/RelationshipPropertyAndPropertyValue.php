<?php

namespace Models;

use Components\DB;

class RelationshipPropertyAndPropertyValue
{
    public static function getRelationshipPropertiesAndPropertyValuesByProduct(\Entities\Product $product)
    {
        $relationship = [];
        if ($product) {
            $dbh = DB::getConnection();
            $sql = "SELECT * FROM ppp WHERE product_id = {$product->getID()}";
            foreach ($dbh->query($sql) as $row) {
                $relationship[] = new \Entities\RelationshipPropertyAndPropertyValue(
                    $row['id'],
                    $row['product_id'],
                    $row['property_id'],
                    $row['property_value_id'],
                    $row['type']
                );
            }
        }
        return $relationship;
    }
}