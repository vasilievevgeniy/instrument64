<?php

namespace Models;

use Components\DB;

class Property
{
    public static function create(\Entities\Property $property, array $categoryIDs)
    {
        if ($property && count($categoryIDs)) {
            $dbh = DB::getConnection();
            $stmt = $dbh->prepare("INSERT INTO property (label, name, sort, active, type, unit) VALUES (:label, :name, :sort, :active, :type, :unit)");
            $stmt->bindParam(':label', $property->getLabel());
            $stmt->bindParam(':name', $property->getName());
            $stmt->bindParam(':sort', $property->getSort());
            $stmt->bindParam(':active', $property->getActive());
            $stmt->bindParam(':type', $property->getType());
            $stmt->bindParam(':unit', $property->getUnit());
            $stmt->execute();

            $propertyID = $dbh->lastInsertId();

            $stmt = $dbh->prepare("INSERT INTO category_property (category_id, property_id) VALUES (:category_id, :property_id)");
            foreach ($categoryIDs as $categoryID) {
                $stmt->bindParam(':category_id', $categoryID);
                $stmt->bindParam(':property_id', $propertyID);
                $stmt->execute();
            }

            return true;
        }
        return false;
    }

    public static function getProperties()
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM property ORDER BY sort ASC";
        $properties = [];
        foreach ($dbh->query($sql) as $row) {
            $properties[$row['id']] = new \Entities\Property(
                $row['id'],
                $row['label'],
                $row['name'],
                $row['sort'],
                $row['active'],
                $row['type']
            );
        }
        return $properties;
    }

    public static function getPropertyByID($id)
    {
        if ($id) {
            $dbh = DB::getConnection();
            $sql = "SELECT * FROM property WHERE id = ? LIMIT 1";
            $query = $dbh->prepare($sql);
            $query->execute([$id]);
            $row = $query->fetch(\PDO::FETCH_OBJ);
            if ($row) {
                $property = new \Entities\Property(
                    $row->id,
                    $row->label,
                    $row->name,
                    $row->sort,
                    $row->active,
                    $row->type,
                    $row->unit
                );
                return $property;
            }
        }
        return false;
    }

    public static function update(\Entities\Property $property, $categoriesIDs)
    {
        if ($property && count($categoriesIDs)) {
            $dbh = DB::getConnection();
            $stmt = $dbh->prepare("UPDATE property SET label = :label, name = :name, sort = :sort, active = :active, type = :type, unit = :unit WHERE id = :id");
            $stmt->bindParam(':id', $property->getID());
            $stmt->bindParam(':label', $property->getLabel());
            $stmt->bindParam(':name', $property->getName());
            $stmt->bindParam(':sort', $property->getSort());
            $stmt->bindParam(':active', $property->getActive());
            $stmt->bindParam(':type', $property->getType());
            $stmt->bindParam(':unit', $property->getUnit());
            $stmt->execute();

            $stmt = $dbh->prepare("DELETE FROM category_property WHERE property_id = :id");
            $stmt->bindParam(':id', $property->getID());
            $stmt->execute();

            foreach ($categoriesIDs as $categoryID) {
                $stmt = $dbh->prepare("INSERT INTO category_property (category_id, property_id) VALUES (:category_id, :property_id)");
                $stmt->bindParam(':category_id', $categoryID);
                $stmt->bindParam(':property_id', $property->getID());
                $stmt->execute();
            }
            return true;
        }
        return false;
    }

    public static function delete($id)
    {
        if (intval($id)) {
            $dbh = DB::getConnection();
            $dbh->beginTransaction();
            $stmt = $dbh->prepare("DELETE FROM category_property WHERE property_id = :id");
            $stmt->bindParam(':id', $id);
            $result1 = $stmt->execute();
            $stmt = $dbh->prepare("DELETE FROM property WHERE id = :id");
            $stmt->bindParam(':id', $id);
            $result2 = $stmt->execute();
            if ($result1 && $result2) {
                $dbh->commit();
                return true;
            } else {
                $dbh->rollBack();
            }
        }
        return false;
    }

    public static function getPropertiesByCategoryID($categoryID)
    {
        $properties = [];
        if ($categoryID) {
            $dbh = DB::getConnection();
            $sql = "SELECT * FROM category_property, property WHERE category_id = ? AND category_property.property_id = property.id ORDER BY sort ASC";
            $query = $dbh->prepare($sql);
            $query->execute([$categoryID]);
            while ($row = $query->fetch(\PDO::FETCH_OBJ)) {
                $properties[$row->id] = new \Entities\Property(
                    $row->id,
                    $row->label,
                    $row->name,
                    $row->soft,
                    $row->active,
                    $row->type,
                    $row->unit
                );
            }
        }
        return $properties;
    }
}
