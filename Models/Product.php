<?php

namespace Models;

use Components\DB;

class Product
{
    public static function create(\Entities\Product $product, array $propertyAndPropertyValues)
    {
        if ($product) {
            $errors = [];
            $dbh = DB::getConnection();
            $dbh->beginTransaction();

            $stmt = $dbh->prepare("INSERT INTO product (label, name, sort, active, category_id, price, brand, weight, length, width, height, image, short_description, full_description, `count`) " .
                "VALUES (:label, :name, :sort, :active, :category_id, :price, :brand, :weight, :length, :width, :height, :image, :short_description, :full_description, :count)");

            $stmt->bindParam(':label', $product->getLabel());
            $stmt->bindParam(':name', $product->getName());
            $stmt->bindParam(':sort', $product->getSort());
            $stmt->bindParam(':active', $product->getActive());
            $stmt->bindParam(':category_id', $product->getCategoryID());
            $stmt->bindParam(':price', $product->getPrice());
            $stmt->bindParam(':brand', $product->getBrand());
            $stmt->bindParam(':weight', $product->getWeight());
            $stmt->bindParam(':length', $product->getLength());
            $stmt->bindParam(':width', $product->getWidth());
            $stmt->bindParam(':height', $product->getHeight());
            $stmt->bindParam(':short_description', $product->getShortDescription());
            $stmt->bindParam(':full_description', $product->getFullDescription());
            $stmt->bindParam(':count', $product->getCount());

            $image = $product->getImage();
            $stmt->bindParam(':image', $image['name']);

            $errors[] = $stmt->execute();
            $productID = $dbh->lastInsertId();

            if (count($propertyAndPropertyValues)) {
                foreach ($propertyAndPropertyValues as $propertyAndPropertyValue) {

                    /** @var \Entities\Property $property1 */
                    $property = array_shift($propertyAndPropertyValue);
                    /** @var \Entities\PropertyValue $propertyValue1 */
                    $propertyValue = array_shift($propertyAndPropertyValue);

                    $stmt = $dbh->prepare("INSERT INTO property_value (value, sort, active) VALUES (:value, :sort, :active)");
                    $stmt->bindParam(':value', $propertyValue->getValue());
                    $stmt->bindParam(':sort', $propertyValue->getSort());
                    $stmt->bindParam(':active', $propertyValue->getActive());
                    $errors[] = $stmt->execute();
                    $propertyValueID = $dbh->lastInsertId();

                    $stmt = $dbh->prepare("INSERT INTO ppp (product_id, property_id, property_value_id, type) VALUES (:product_id, :property_id, :property_value_id, :type)");
                    $stmt->bindParam(':product_id', $productID);
                    $stmt->bindParam(':property_id', $property->getID());
                    $stmt->bindParam(':property_value_id', $propertyValueID);
                    $stmt->bindParam(':type', $property->getType());
                    $errors[] = $stmt->execute();
                }
            }

            if (!in_array(false, $errors)) {
                $dbh->commit();

                if (count($product->getImage())) {
                    $image = $product->getImage();
                    move_uploaded_file($image['tmp_name'], ROOT . '/upload/' . $image['name']);
                }

                return true;
            }
            $dbh->rollBack();

        }
        return false;
    }

    public static function getProducts()
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM product ORDER BY sort DESC";
        $products = [];
        foreach ($dbh->query($sql) as $row) {
            $products[$row['id']] = new \Entities\Product(
                $row['id'],
                $row['label'],
                $row['name'],
                $row['sort'],
                $row['active'],
                $row['category_id'],
                $row['brand'],
                $row['price'],
                $row['weight'],
                $row['length'],
                $row['width'],
                $row['height'],
                $row['image'],
                $row['short_description'],
                $row['full_description'],
                $row['count']
            );
        }
        return $products;
    }

    public static function getProductsCount($count)
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM product ORDER BY sort DESC LIMIT $count";
        $products = [];
        foreach ($dbh->query($sql) as $row) {
            $products[$row['id']] = new \Entities\Product(
                $row['id'],
                $row['label'],
                $row['name'],
                $row['sort'],
                $row['active'],
                $row['category_id'],
                $row['brand'],
                $row['price'],
                $row['weight'],
                $row['length'],
                $row['width'],
                $row['height'],
                $row['image'],
                $row['short_description'],
                $row['full_description'],
                $row['count']
            );
        }
        return $products;
    }

    public static function getProductsByCategories()
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM product ORDER BY sort ASC";
        $products = [];
        foreach ($dbh->query($sql) as $row) {
            $products[$row['category_id']][] = new \Entities\Product(
                $row['id'],
                $row['label'],
                $row['name'],
                $row['sort'],
                $row['active'],
                $row['category_id'],
                $row['brand'],
                $row['price'],
                $row['weight'],
                $row['length'],
                $row['width'],
                $row['height'],
                $row['image'],
                $row['short_description'],
                $row['full_description'],
                $row['count']
            );
        }
        return $products;
    }

    public static function getProductsByCategoryID($id)
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM product WHERE category_id = $id ORDER BY sort DESC";
        $products = [];
        foreach ($dbh->query($sql) as $row) {
            $products[$row['id']] = new \Entities\Product(
                $row['id'],
                $row['label'],
                $row['name'],
                $row['sort'],
                $row['active'],
                $row['category_id'],
                $row['brand'],
                $row['price'],
                $row['weight'],
                $row['length'],
                $row['width'],
                $row['height'],
                $row['image'],
                $row['short_description'],
                $row['full_description'],
                $row['count']
            );
        }
        return $products;
    }

    public static function getProductByID($id)
    {
        if ($id) {
            $dbh = DB::getConnection();
            $sql = "SELECT * FROM product WHERE id = ? LIMIT 1";
            $query = $dbh->prepare($sql);
            $query->execute([$id]);
            $row = $query->fetch(\PDO::FETCH_OBJ);
            if ($row) {
                $product = new \Entities\Product(
                    $row->id,
                    $row->label,
                    $row->name,
                    $row->sort,
                    $row->active,
                    $row->category_id,
                    $row->brand,
                    $row->price,
                    $row->weight,
                    $row->length,
                    $row->width,
                    $row->height,
                    $row->image,
                    $row->short_description,
                    $row->full_description,
                    $row->count
                );
                return $product;
            }
        }
        return false;
    }

    public static function updateCount(\Entities\Product $product)
    {
        if ($product) {
            $dbh = DB::getConnection();
            $stmt = $dbh->prepare("UPDATE product SET `count` = :count WHERE id = :id");
            $stmt->bindParam(':count', $product->getCount());
            $stmt->bindParam(':id', $product->getID());
            $stmt->execute();
        }
    }

    public static function update(\Entities\Product $product, array $propertyAndPropertyValues)
    {
        if ($product) {
            $errors = [];
            $dbh = DB::getConnection();
            $dbh->beginTransaction();

            $stmt = $dbh->prepare("UPDATE product SET label = :label, name = :name, sort = :sort, active = :active, category_id = :category_id, price = :price, brand = :brand, weight = :weight, length = :length, width = :width, height = :height, short_description = :short_description, full_description = :full_description, `count` = :count WHERE id = :id");
            $stmt->bindParam(':id', $product->getID());
            $stmt->bindParam(':label', $product->getLabel());
            $stmt->bindParam(':name', $product->getName());
            $stmt->bindParam(':sort', $product->getSort());
            $stmt->bindParam(':active', $product->getActive());
            $stmt->bindParam(':category_id', $product->getCategoryID());
            $stmt->bindParam(':price', $product->getPrice());
            $stmt->bindParam(':brand', $product->getBrand());
            $stmt->bindParam(':weight', $product->getWeight());
            $stmt->bindParam(':length', $product->getLength());
            $stmt->bindParam(':width', $product->getWidth());
            $stmt->bindParam(':height', $product->getHeight());
            $stmt->bindParam(':short_description', $product->getShortDescription());
            $stmt->bindParam(':full_description', $product->getFullDescription());
            $stmt->bindParam(':count', $product->getCount());
            $errors[] = $stmt->execute();

            $stmt = $dbh->prepare("DELETE FROM ppp WHERE product_id = :id");
            $stmt->bindParam(':id', $product->getID());
            $errors[] = $stmt->execute();

            if (count($propertyAndPropertyValues)) {
                foreach ($propertyAndPropertyValues as $propertyAndPropertyValue) {

                    /** @var \Entities\Property $property1 */
                    $property = array_shift($propertyAndPropertyValue);
                    /** @var \Entities\PropertyValue $propertyValue1 */
                    $propertyValue = array_shift($propertyAndPropertyValue);

                    $stmt = $dbh->prepare("INSERT INTO property_value (value, sort, active) VALUES (:value, :sort, :active)");
                    $stmt->bindParam(':value', $propertyValue->getValue());
                    $stmt->bindParam(':sort', $propertyValue->getSort());
                    $stmt->bindParam(':active', $propertyValue->getActive());
                    $errors[] = $stmt->execute();
                    $propertyValueID = $dbh->lastInsertId();

                    $stmt = $dbh->prepare("INSERT INTO ppp (product_id, property_id, property_value_id, type) VALUES (:product_id, :property_id, :property_value_id, :type)");
                    $stmt->bindParam(':product_id', $product->getID());
                    $stmt->bindParam(':property_id', $property->getID());
                    $stmt->bindParam(':property_value_id', $propertyValueID);
                    $stmt->bindParam(':type', $property->getType());
                    $errors[] = $stmt->execute();
                }
            }

            if (!in_array(false, $errors)) {
                $dbh->commit();
                return true;
            }
            $dbh->rollBack();

        }
        return false;
    }

    public static function delete($id)
    {
        if ($id) {
            $dbh = DB::getConnection();
            $dbh->beginTransaction();
            $errors = [];

            $stmt = $dbh->prepare("DELETE FROM ppp WHERE product_id = :id");
            $stmt->bindParam(':id', $id);
            $errors[] = $stmt->execute();

            $stmt = $dbh->prepare("DELETE FROM product WHERE id = :id");
            $stmt->bindParam(':id', $id);
            $errors[] = $stmt->execute();

            if (!in_array(false, $errors)) {
                $dbh->commit();
                return true;
            }
            $dbh->rollBack();
        }
        return false;
    }

    public static function getProdustsByIds($idsArray)
    {
        // Соединение с БД
        $db = DB::getConnection();

        // Превращаем массив в строку для формирования условия в запросе
        $idsString = implode(',', $idsArray);

        // Текст запроса к БД
        $sql = "SELECT * FROM product WHERE active = 1 AND id IN ($idsString)";

        $result = $db->query($sql);

        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(\PDO::FETCH_ASSOC);

        // Получение и возврат результатов
        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['label'] = $row['label'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['sort'] = $row['sort'];
            $products[$i]['active'] = $row['active'];
            $products[$i]['price'] = $row['price'];
            $i++;
        }
        return $products;
    }

    public static function getBrands($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT brand FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $i = 0;
        $brands = [];
        while ($row = $result->fetch()) {
            $brands[$row['brand']] = $row['brand'];
            $i++;
        }
        return $brands;
    }

    public static function getMaxWeight($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT max(weight) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['max(weight)'];
    }

    public static function getMinWeight($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT min(weight) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['min(weight)'];
    }

    public static function getMaxLength($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT max(length) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['max(length)'];
    }

    public static function getMinLength($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT min(length) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['min(length)'];
    }

    public static function getMaxWidth($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT max(width) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['max(width)'];
    }

    public static function getMinWidth($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT min(width) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['min(width)'];
    }

    public static function getMaxHeight($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT max(height) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['max(height)'];
    }

    public static function getMinHeight($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT min(height) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['min(height)'];
    }

    public static function getMaxPrice($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT max(price) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['max(price)'];
    }

    public static function getMinPrice($categoryID)
    {
        $db = DB::getConnection();
        $sql = "SELECT min(price) FROM product WHERE active = 1 AND category_id = $categoryID";
        $result = $db->query($sql);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $row = $result->fetch();
        return $row['min(price)'];
    }

    public static function getProductsByFilter($filters, $categoryID)
    {
        $filterProperties = [];

        foreach ($filters as $filterK => $filterV) {
            if (strpos($filterK, 'property_') === 0) {
                $filterProperties[$filterK] = $filterV;
            }
        }

        $filterPropertiesSQL = '';


        $joinSQL = '';
        $andSQL = '';
        $i = 0;
        foreach ($filterProperties as $filterPropertyK => $filterProperty) {
            $i++;


            $joinSQL .= " inner join ppp as ppp{$i} on ppp{$i}.product_id = product.id
                inner join property as prop{$i} on prop{$i}.id = ppp{$i}.property_id
                inner join property_value as pv{$i} on pv{$i}.id = ppp{$i}.property_value_id ";

            $filterPropertyKClear = str_replace('property_', '', $filterPropertyK);

            if (is_array($filterProperty)) {



                $filterProperty1 = [];
                foreach ($filterProperty as $filterP) {
                    $filterProperty1[] = "'" . $filterP . "'";
                }

                $filterPropertySQL = implode(',', $filterProperty1);

                $andSQL .= " and (pv{$i}.value in ($filterPropertySQL) and prop{$i}.name = '{$filterPropertyKClear}') ";


            } else {

                if (strpos($filterProperty, ' - ')) {

                    $maxMin = explode(' - ', $filterProperty);
                    $min = array_shift($maxMin);
                    $max = array_shift($maxMin);


                    $andSQL .= " and (pv{$i}.value >= $min and pv{$i}.value <= $max and prop{$i}.name = '{$filterPropertyKClear}') ";


                } else if ($filterProperty == 'all' || $filterProperty == 'yes' || $filterProperty = 'no') {

                    if ($filterProperty == 'all') {
                        $andSQL .= " and (pv{$i}.value in ('0', '1') and prop{$i}.name = '{$filterPropertyKClear}') ";
                    } else if ($filterProperty == 'yes') {
                        $andSQL .= " and (pv{$i}.value in ('1') and prop{$i}.name = '{$filterPropertyKClear}') ";
                    } else if ($filterProperty == 'no') {
                        $andSQL .= " and (pv{$i}.value in ('0') and prop{$i}.name = '{$filterPropertyKClear}') ";
                    }


                }

            }
        }

        $brands = $filters['brand'];
        $brandSQL = '';
        if (count($brands)) {
            foreach ($brands as $brandK => $brandsV) {
                $brands[$brandK] = "'" . $brandsV . "'";
            }
            $brandSQL = implode(',', $brands);
            $brandSQL = " AND brand IN ($brandSQL)";
        }

        /**/

        $price = explode(' - ', $filters['price']);
        $priceMin = array_shift($price);
        $priceMinSQL = '';
        if ($priceMin) {
            $priceMinSQL = " AND price >= $priceMin";
        }

        $priceMax = array_shift($price);
        $priceMaxSQL = '';
        if ($priceMax) {
            $priceMaxSQL = " AND price <= $priceMax";
        }

        /**/

        $weight = explode(' - ', $filters['weight']);
        $weightMin = array_shift($weight);
        $weightMinSQL = '';
        if ($weightMin) {
            $weightMinSQL = " AND weight >= $weightMin";
        }

        $weightMax = array_shift($weight);
        $weightMaxSQL = '';
        if ($weightMax) {
            $weightMaxSQL = " AND weight <= $weightMax";
        }

        /**/

        $length = explode(' - ', $filters['length']);
        $lengthMin = array_shift($length);
        $lengthMinSQL = '';
        if ($lengthMin) {
            $lengthMinSQL = " AND length >= $lengthMin";
        }

        $lengthMax = array_shift($length);
        $lengthMaxSQL = '';
        if ($lengthMax) {
            $lengthMaxSQL = " AND length <= $lengthMax";
        }

        /**/

        $width = explode(' - ', $filters['width']);
        $widthMin = array_shift($width);
        $widthMinSQL = '';
        if ($widthMin) {
            $widthMinSQL = " AND width >= $widthMin";
        }

        $widthMax = array_shift($width);
        $widthMaxSQL = '';
        if ($widthMax) {
            $widthMaxSQL = " AND width <= $widthMax";
        }

        /**/

        $height = explode(' - ', $filters['height']);
        $heightMin = array_shift($height);
        $heightMinSQL = '';
        if ($heightMin) {
            $heightMinSQL = " AND height >= $heightMin";
        }

        $heightMax = array_shift($height);
        $heightMaxSQL = '';
        if ($heightMax) {
            $heightMaxSQL = " AND height <= $heightMax";
        }

        /**/

        $dbh = DB::getConnection();

        $sql = "SELECT product.* FROM product $joinSQL 
        WHERE product.active = 1 AND category_id = $categoryID 
        $priceMinSQL $priceMaxSQL $brandSQL $weightMinSQL $weightMaxSQL $lengthMinSQL $lengthMaxSQL $widthMinSQL $widthMaxSQL $heightMinSQL $heightMaxSQL $andSQL";

        $products = [];
        foreach ($dbh->query($sql) as $row) {
            $products[$row['id']] = new \Entities\Product(
                $row['id'],
                $row['label'],
                $row['name'],
                $row['sort'],
                $row['active'],
                $row['category_id'],
                $row['brand'],
                $row['price'],
                $row['weight'],
                $row['length'],
                $row['width'],
                $row['height'],
                $row['image'],
                $row['short_description'],
                $row['full_description'],
                $row['count']
            );
        }
        return $products;
    }

    public static function getProductByLabelSearch($search)
    {
        if ($search) {
            $dbh = DB::getConnection();
            $sql = "SELECT * FROM product WHERE label LIKE '%$search' OR label LIKE '$search%' OR label LIKE '%$search%' OR label = '$search'";
            $products = [];
            foreach ($dbh->query($sql) as $row) {
                $products[$row['id']] = new \Entities\Product(
                    $row['id'],
                    $row['label'],
                    $row['name'],
                    $row['sort'],
                    $row['active'],
                    $row['category_id'],
                    $row['brand'],
                    $row['price'],
                    $row['weight'],
                    $row['length'],
                    $row['width'],
                    $row['height'],
                    $row['image'],
                    $row['short_description'],
                    $row['full_description'],
                    $row['count']
                );
            }
            return $products;
        }
        return false;
    }
}