<?php

namespace Models;

use Components\DB;

class PropertyValue
{
    public static function getPropertyValuesByPropertiesAndProduct(array $properties, \Entities\Product $product)
    {
        $propertyValues = [];

        if (count($properties) && $product) {

            $propertyIDs = [];
            foreach ($properties as $property) {
                $propertyIDs[$property->getID()] = $property->getID();
            }
            $propertyIDs = implode(',', $propertyIDs);

            $dbh = DB::getConnection();
            $sql = "SELECT property_value_id, type FROM ppp WHERE property_id IN ($propertyIDs) AND product_id = {$product->getID()}";

            foreach ($dbh->query($sql) as $row) {
                $sql = "SELECT * FROM property_value WHERE id = {$row['property_value_id']}";

                foreach ($dbh->query($sql) as $row1) {
                    $propertyValues[] = new \Entities\PropertyValue(
                        $row1['id'],
                        $row1['value'],
                        $row1['sort'],
                        $row1['active']
                    );
                }
            }
        }
        return $propertyValues;
    }
}