<?php

namespace Models;

use Components\DB;

class Category
{
    public static function create(\Entities\Category $category)
    {
        if ($category) {
            $dbh = DB::getConnection();
            $stmt = $dbh->prepare("INSERT INTO category (label, name, sort, active) VALUES (:label, :name, :sort, :active)");
            $stmt->bindParam(':label', $category->getLabel());
            $stmt->bindParam(':name', $category->getName());
            $stmt->bindParam(':sort', $category->getSort());
            $stmt->bindParam(':active', $category->getActive());
            return $stmt->execute();
        }
        return false;
    }

    public static function getCategories()
    {
        $dbh = DB::getConnection();
        $sql = "SELECT * FROM category ORDER BY sort ASC";
        $categories = [];
        foreach ($dbh->query($sql) as $row) {
            $categories[$row['id']] = new \Entities\Category(
                $row['id'],
                $row['label'],
                $row['name'],
                $row['sort'],
                $row['active']
            );
        }
        return $categories;
    }

    /**
     * @param $id
     * @return bool|\Entities\Category
     */
    public static function getCategoryByID($id)
    {
        if (intval($id)) {
            $dbh = DB::getConnection();
            $sql = "SELECT * FROM category WHERE id = ? LIMIT 1";
            $query = $dbh->prepare($sql);
            $query->execute([$id]);
            $row = $query->fetch(\PDO::FETCH_OBJ);
            if ($row) {
                $category = new \Entities\Category(
                    $row->id,
                    $row->label,
                    $row->name,
                    $row->sort,
                    $row->active
                );
                return $category;
            }
        }
        return false;
    }

    public static function update(\Entities\Category $category)
    {
        if ($category) {
            $dbh = DB::getConnection();
            $stmt = $dbh->prepare("UPDATE category SET label = :label, name = :name, sort = :sort, active = :active WHERE id = :id");
            $stmt->bindParam(':id', $category->getID());
            $stmt->bindParam(':label', $category->getLabel());
            $stmt->bindParam(':name', $category->getName());
            $stmt->bindParam(':sort', $category->getSort());
            $stmt->bindParam(':active', $category->getActive());
            return $stmt->execute();
        }
        return false;
    }

    public static function delete($id)
    {
        if ($id) {
            $dbh = DB::getConnection();
            $stmt = $dbh->prepare("DELETE FROM category WHERE id = :id");
            $stmt->bindParam(':id', $id);
            return $stmt->execute();
        }
        return false;
    }

    public static function getCategoriesByPropertyID($propertyID)
    {
        if ($propertyID) {
            $dbh = DB::getConnection();
            $sql = "SELECT * FROM category_property, category WHERE property_id = ? AND category_property.category_id = category.id ORDER BY sort ASC";
            $query = $dbh->prepare($sql);
            $query->execute([$propertyID]);
            $categories = [];
            while ($row = $query->fetch(\PDO::FETCH_OBJ)) {
                $categories[$row->id] = new \Entities\Category(
                    $row->id,
                    $row->label,
                    $row->name,
                    $row->soft,
                    $row->active
                );
            }
            return $categories;
        }
        return false;
    }
}
