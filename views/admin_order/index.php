<?php

/** @var $ordersList[] */
/** @var $errors[] */

use Models\Order;

include ROOT . '/templates/layouts/admin_header.php'; ?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/category/">Управление категориями</a></li>
                <li><a href="/admin/property/">Управление свойствами</a></li>
                <li><a href="/admin/product/">Управление продуктами</a></li>
                <li class="uk-active"><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/admin/">Администратор</a></li>
            <li class="uk-active"><span>Управление заказами</span></li>
        </ul>

        <h1 class="uk-panel-title">Просмотр заказов</h1>

        <?if(count($ordersList)):?>

            <table class="uk-table">
                <thead>
                <tr>
                    <th>ID заказа</th>
                    <th>Имя покупателя</th>
                    <th>Телефон покупателя</th>
                    <th>Дата оформления</th>
                    <th>Статус заказа</th>
                    <th>Управление заказом</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($ordersList as $order): ?>
                    <tr>
                        <td>
                            <a href="/admin/order/view/<?php echo $order['id']; ?>/">
                                <?php echo $order['id']; ?>
                            </a>
                        </td>
                        <td><?php echo $order['user_name']; ?></td>
                        <td><?php echo $order['user_phone']; ?></td>
                        <td><?php echo $order['date']; ?></td>
                        <td><?php echo Order::getStatusText($order['status']); ?></td>
                        <td>
                            <div><a class="uk-button uk-button-primary uk-margin-small-bottom" href="/admin/order/view/<?php echo $order['id']; ?>/" title="Смотреть">Просмотр</a></div>
                            <div><a class="uk-button uk-button-success uk-margin-small-bottom" href="/admin/order/update/<?php echo $order['id']; ?>/" title="Редактировать">Редактирование</a></div>
                            <div><a class="uk-button uk-button-danger" href="/admin/order/delete/<?php echo $order['id']; ?>/" title="Удалить">Удалить</a></div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?else:?>

            <div class="uk-alert uk-alert-danger">Нет заказов</div>

        <?endif;?>

    </div>
</div>

<?php include ROOT . '/templates/layouts/admin_footer.php'; ?>