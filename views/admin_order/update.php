<?php include ROOT . '/templates/layouts/admin_header.php'; ?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/category/">Управление категориями</a></li>
                <li><a href="/admin/property/">Управление свойствами</a></li>
                <li><a href="/admin/product/">Управление продуктами</a></li>
                <li class="uk-active"><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">
        <ul class="uk-breadcrumb">
            <li><a href="/admin/">Администратор</a></li>
            <li><a href="/admin/order/">Управление заказами</a></li>
            <li class="uk-active"><span>Редактировать заказ</span></li>
        </ul>
        <h1 class="uk-panel-title">Редактировать заказ #<?php echo $id; ?></h1>
        <form action="" method="post">
            <div class="uk-form uk-form-stacked">
                <div class="uk-form-row">
                    <label class="uk-form-label">Имя клиента</label>
                    <div class="uk-form-controls">
                        <input type="text" name="userName" placeholder="" value="<?php echo $order['user_name']; ?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Телефон клиента</label>
                    <div class="uk-form-controls">
                        <input type="text" name="userPhone" placeholder="" value="<?php echo $order['user_phone']; ?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Комментарий клиента</label>
                    <div class="uk-form-controls">
                        <input type="text" name="userComment" placeholder="" value="<?php echo $order['user_comment']; ?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Дата оформления заказа</label>
                    <div class="uk-form-controls">
                        <input type="text" name="date" placeholder="" value="<?php echo $order['date']; ?>">
                    </div>
                </div>
                <div class="uk-form-row">
                    <label class="uk-form-label">Статус</label>
                    <div class="uk-form-controls">
                        <select name="status">
                            <option value="1" <?php if ($order['status'] == 1) echo ' selected="selected"'; ?>>Новый заказ</option>
                            <option value="2" <?php if ($order['status'] == 2) echo ' selected="selected"'; ?>>В обработке</option>
                            <option value="3" <?php if ($order['status'] == 3) echo ' selected="selected"'; ?>>Доставляется</option>
                            <option value="4" <?php if ($order['status'] == 4) echo ' selected="selected"'; ?>>Закрыт</option>
                        </select>
                    </div>
                </div>
            </div>
            <input type="submit" name="submit" class="btn btn-default uk-button uk-button-success uk-margin-top" value="Сохранить">
        </form>
    </div>
</div>

<?php include ROOT . '/templates/layouts/admin_footer.php'; ?>

