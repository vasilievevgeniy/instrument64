<?php include ROOT . '/templates/layouts/admin_header.php'; ?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/category/">Управление категориями</a></li>
                <li><a href="/admin/property/">Управление свойствами</a></li>
                <li><a href="/admin/product/">Управление продуктами</a></li>
                <li class="uk-active"><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/admin/">Администратор</a></li>
            <li><a href="/admin/order/">Управление заказами</span></a></li>
            <li class="uk-active"><span>Удалить заказ</span></li>
        </ul>

        <h1 class="uk-panel-title">Удалить заказ #<?php echo $id; ?></h1>

        <p>Вы действительно хотите удалить этот заказ?</p>

        <form method="post">
            <input class="uk-button uk-button-danger" type="submit" name="submit" value="Удалить" />
        </form>
    </div>
</div>

<?php include ROOT . '/templates/layouts/admin_footer.php'; ?>

