<?

/** @var Entities\Category[] $categories */
/** @var $errors[] */

require_once ROOT . '/templates/layouts/header.php';?>

    <div class="uk-grid uk-grid-medium">
        <div class="uk-width-1-4">
            <img class="uk-margin-bottom" src="/templates/images/10.jpg">
            <?if(count($categories)):?>
                <div class="uk-panel-box">
                    <h3 class="uk-panel-title">Категории</h3>
                    <ul class="uk-nav uk-nav-side">
                        <?foreach($categories as $category):?>
                            <?if($category->getActive()):?>
                                <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </div>
            <?endif;?>
            <img class="uk-margin-top" src="/templates/images/11.jpg">
        </div>
        <div class="uk-width-3-4">

            <ul class="uk-breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="uk-active"><span>Обратная связь</span></li>
            </ul>

            <h3 class="uk-panel-title">Обратная связь</h3>

            <?if($send):?>

                <div class="uk-alert uk-alert-success">Сообщение было отправлено</div>

            <?else:?>

                <?foreach($errors as $error):?>
                    <div class="uk-alert uk-alert-danger"><?=$error?></div>
                <?endforeach;?>

                <form method="post">
                    <div class="uk-form uk-form-stacked">
                        <div class="uk-form-row">
                            <label class="uk-form-label">Ваше имя *</label>
                            <div class="uk-form-controls">
                                <input type="text" name="name" placeholder="Ваше имя" value="<?=$name?>">
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <label class="uk-form-label">Ваш телефон *</label>
                            <div class="uk-form-controls">
                                <input type="text" name="phone" placeholder="Ваш телефон" value="<?=$phone?>">
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <label class="uk-form-label">Ваш e-mail *</label>
                            <div class="uk-form-controls">
                                <input type="email" name="email" placeholder="Ваш e-mail" value="<?=$email?>">
                            </div>
                        </div>
                        <div class="uk-form-row">
                            <label class="uk-form-label">Ваше сообщение *</label>
                            <div class="uk-form-controls">
                                <textarea name="message"><?=$message?></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="submit" name="feedback" class="uk-button uk-button-success uk-margin-top">
                </form>

            <?endif;?>
        </div>
    </div>

<?require_once ROOT . '/templates/layouts/footer.php';?>