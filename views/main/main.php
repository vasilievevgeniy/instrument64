<?

/** @var Entities\Category[] $categories */

require_once ROOT . '/templates/layouts/header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <img class="uk-margin-bottom" src="/templates/images/10.jpg">
        <?if(count($categories)):?>
            <div class="uk-panel-box">
                <h3 class="uk-panel-title">Категории</h3>
                <ul class="uk-nav uk-nav-side">
                    <?foreach($categories as $category):?>
                        <?if($category->getActive()):?>
                            <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                        <?endif;?>
                    <?endforeach;?>
                </ul>
            </div>
        <?endif;?>
        <img class="uk-margin-top" src="/templates/images/11.jpg">
    </div>
    <div class="uk-width-3-4">
        <div class="uk-panel-box uk-margin-bottom">
            <div class="uk-slidenav-position" data-uk-slideshow="{kenburns:true}">
                <ul class="uk-slideshow">
                    <li><img src="/templates/images/aYEcRjZ0nb8.jpg" width="" height="" alt=""></li>
                    <li><img src="/templates/images/L-zr1zntKAI.jpg" width="" height="" alt=""></li>
                    <li><img src="/templates/images/c5ReuRs4cAs.jpg" width="" height="" alt=""></li>
                </ul>
                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
                    <li data-uk-slideshow-item="0"><a href=""></a></li>
                    <li data-uk-slideshow-item="1"><a href=""></a></li>
                    <li data-uk-slideshow-item="2"><a href=""></a></li>
                </ul>
            </div>
        </div>
        <h3 class="uk-panel-title">Товары</h3>
        <div class="uk-grid uk-grid-medium">
            <?require_once ROOT . '/templates/layouts/product_item.php'?>
        </div>
    </div>
</div>

<?require_once ROOT . '/templates/layouts/footer.php';?>