<?

/** @var Entities\Category[] $categories */

require_once ROOT . '/templates/layouts/header.php';?>

    <div class="uk-grid uk-grid-medium">
        <div class="uk-width-1-4">
            <img class="uk-margin-bottom" src="/templates/images/10.jpg">
            <?if(count($categories)):?>
                <div class="uk-panel-box">
                    <h3 class="uk-panel-title">Категории</h3>
                    <ul class="uk-nav uk-nav-side">
                        <?foreach($categories as $category):?>
                            <?if($category->getActive()):?>
                                <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </div>
            <?endif;?>
            <img class="uk-margin-top" src="/templates/images/11.jpg">
        </div>
        <div class="uk-width-3-4">

            <ul class="uk-breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="uk-active"><span>Контакты</span></li>
            </ul>

            <h3 class="uk-panel-title">Контакты</h3>

            <p><i class="uk-icon-phone"></i> +7 (987) 358-41-92</p>

            <p><i class="uk-icon-map-o"></i> г. Саратов, ул. Огородная, д. 158а</p>

            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A2bb56c9f802c4cf0dc37f4681e94ac6939c6d8f5965efc721c0816368187e789&amp;width=840&amp;height=500&amp;lang=ru_RU&amp;scroll=true"></script>

        </div>
    </div>

<?require_once ROOT . '/templates/layouts/footer.php';?>