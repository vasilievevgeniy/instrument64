<?

/** @var Entities\Category[] $categories */

require_once ROOT . '/templates/layouts/header.php';?>

    <div class="uk-grid uk-grid-medium">
        <div class="uk-width-1-4">
            <img class="uk-margin-bottom" src="/templates/images/10.jpg">
            <?if(count($categories)):?>
                <div class="uk-panel-box">
                    <h3 class="uk-panel-title">Категории</h3>
                    <ul class="uk-nav uk-nav-side">
                        <?foreach($categories as $category):?>
                            <?if($category->getActive()):?>
                                <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </div>
            <?endif;?>
            <img class="uk-margin-top" src="/templates/images/11.jpg">
        </div>
        <div class="uk-width-3-4">

            <ul class="uk-breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="uk-active"><span>О нас</span></li>
            </ul>

            <h3 class="uk-panel-title">О нас</h3>

            <p><b>Инструмент-64</b> – крупнейшая сеть магазинов инструментов и оборудования в г. Саратове.</p><p> В линейку продукции входит более 20 брендов – от легендарных и известных во всем мире, например, Bosch, DeWalt, Makita, до менее популярных торговых марок, как Patriot, Ryobi и других.</p>

        </div>
    </div>

<?require_once ROOT . '/templates/layouts/footer.php';?>