<?php

/** @var Entities\Category[] $categories */
/** @var $errors[] */
/** @var $name string */
/** @var $email string */
/** @var $password string */
/** @var $phone string */

include ROOT . '/templates/layouts/header.php'; ?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <img class="uk-margin-bottom" src="/templates/images/10.jpg">
        <?if(count($categories)):?>
            <div class="uk-panel-box">
                <h3 class="uk-panel-title">Категории</h3>
                <ul class="uk-nav uk-nav-side">
                    <?foreach($categories as $category):?>
                        <?if($category->getActive()):?>
                            <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                        <?endif;?>
                    <?endforeach;?>
                </ul>
            </div>
        <?endif;?>
        <img class="uk-margin-top" src="/templates/images/11.jpg">
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="uk-active"><span>Регистрация</span></li>
        </ul>

        <h3 class="uk-panel-title">Регистрация</h3>

        <?php if ($result): ?>
            <div class="uk-alert uk-alert-success">Вы зарегистрированы!</div>
        <?else:?>

            <?if($errors):?>
                <?foreach($errors as $error):?>
                    <div class="uk-alert uk-alert-danger"><?=$error?></div>
                <?endforeach;?>
            <?endif;?>

            <div>
                <form action="" method="post" >
                    <span class="uk-form">
                        <input style="width: 140px; "type="text" name="name" placeholder="Имя" value="<?php echo $name; ?>"/>
                        <input style="width: 140px;" type="email" name="email" placeholder="E-mail" value="<?php echo $email; ?>"/>
                        <input style="width: 140px;" type="text" name="phone" placeholder="Телефон" value="<?php echo $phone; ?>" id="phone"/>
                        <script>
                            jQuery(function($){
                                $("#phone").mask("+7 (999) 999-99-99");
                            });
                        </script>
                        <input style="width: 140px;" type="password" name="password" placeholder="Пароль" value="<?php echo $password; ?>"/>
                    </span>
                    <input type="submit" name="submit" class="uk-button uk-button-success" value="Регистрация">
                </form>
            </div>

        <?endif;?>

    </div>
</div>

<?php include ROOT . '/templates/layouts/footer.php'; ?>
