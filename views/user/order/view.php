<?php

use Models\Order;

include ROOT . '/templates/layouts/header.php'; ?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <img class="uk-margin-bottom" src="/templates/images/10.jpg">
        <?if(count($categories)):?>
            <div class="uk-panel uk-panel-box">
                <h3 class="uk-panel-title">Категории</h3>
                <ul class="uk-nav uk-nav-side">
                    <?foreach($categories as $category):?>
                        <?if($category->getActive()):?>
                            <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                        <?endif;?>
                    <?endforeach;?>
                </ul>
            </div>
        <?endif;?>
        <img class="uk-margin-top" src="/templates/images/11.jpg">
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/user/order/">Просмотр заказов</a></li>
            <li class="uk-active"><span>Просмотр заказа</span></li>
        </ul>

        <h1 class="uk-panel-title">Просмотр заказа #<?php echo $order['id']; ?></h1>

        <p>Информация о заказе:</p>
        <table class="uk-table">
            <tr>
                <td>Номер заказа</td>
                <td><?php echo $order['id']; ?></td>
            </tr>
            <tr>
                <td>Имя клиента</td>
                <td><?php echo $order['user_name']; ?></td>
            </tr>
            <tr>
                <td>Телефон клиента</td>
                <td><?php echo $order['user_phone']; ?></td>
            </tr>
            <tr>
                <td>Комментарий клиента</td>
                <td><?php echo $order['user_comment']; ?></td>
            </tr>
            <?php if ($order['user_id'] != 0): ?>
                <tr>
                    <td>ID клиента</td>
                    <td><?php echo $order['user_id']; ?></td>
                </tr>
            <?php endif; ?>
            <tr>
                <td>Статус заказа</td>
                <td><?php echo Order::getStatusText($order['status']); ?></td>
            </tr>
            <tr>
                <td>Дата заказа</td>
                <td><?php echo $order['date']; ?></td>
            </tr>
        </table>

        <h5>Товары в заказе:</h5>

        <table class="uk-table">
            <tr>
                <th>Название</th>
                <th>Цена</th>
                <th>Количество</th>
            </tr>
            <?php foreach ($products as $product): ?>
                <tr>
                    <td><?php echo $product['label']; ?></td>
                    <td><?php echo $product['price']; ?> <i class="uk-icon-rub"></i></td>
                    <td><?php echo $productsQuantity[$product['id']]; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
</div>

<?php include ROOT . '/templates/layouts/footer.php'; ?>
