<?

/** @var Entities\Category[] $categories */
/** @var $errors[] */
/** @var $email string */
/** @var $password string */

require_once ROOT . '/templates/layouts/header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <img class="uk-margin-bottom" src="/templates/images/10.jpg">
        <?if(count($categories)):?>
            <div class="uk-panel-box">
                <h3 class="uk-panel-title">Категории</h3>
                <ul class="uk-nav uk-nav-side">
                    <?foreach($categories as $category):?>
                        <?if($category->getActive()):?>
                            <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                        <?endif;?>
                    <?endforeach;?>
                </ul>
            </div>
        <?endif;?>
        <img class="uk-margin-top" src="/templates/images/11.jpg">
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="uk-active"><span>Авторизация</span></li>
        </ul>

        <h3 class="uk-panel-title">Авторизация</h3>

        <?if(count($errors)):?>
            <?foreach($errors as $error):?>
                <div class="uk-alert uk-alert-danger"><?=$error?></div>
            <?endforeach;?>
        <?endif;?>

        <div>
            <form action="" method="post">
                <span class="uk-form">
                    <input type="email" name="email" placeholder="E-mail" value="<?php echo $email; ?>"/>
                    <input type="password" name="password" placeholder="Пароль" value="<?php echo $password; ?>"/>
                </span>
                <input type="submit" name="submit" class="uk-button uk-button-success" value="Вход" />
            </form>
        </div>

    </div>
</div>

<?require_once ROOT . '/templates/layouts/footer.php';?>
