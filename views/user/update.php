<?php
/** @var \Entities\Category[] $categories */
/** @var $errors[] */
/** @var $user array */
include ROOT . '/templates/layouts/header.php'; ?>
<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <img class="uk-margin-bottom" src="/templates/images/10.jpg">
        <?if(count($categories)):?>
            <div class="uk-panel-box">
                <h3 class="uk-panel-title">Категории</h3>
                <ul class="uk-nav uk-nav-side">
                    <?foreach($categories as $category):?>
                        <?if($category->getActive()):?>
                            <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                        <?endif;?>
                    <?endforeach;?>
                </ul>
            </div>
        <?endif;?>
        <img class="uk-margin-top" src="/templates/images/11.jpg">
    </div>
    <div class="uk-width-3-4">
        <ul class="uk-breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="uk-active"><span>Личный кабинет</span></li>
        </ul>
        <h3 class="uk-panel-title">Личный кабинет</h3>
        <?if($result):?>
            <div class="uk-alert uk-alert-success">Данные обновлены!</div>
        <?else:?>
            <?if($errors):?>
                <?foreach($errors as $error):?>
                    <div class="uk-alert uk-alert-danger"><?=$error?></div>
                <?endforeach;?>
            <?endif;?>
            <div>
                <form action="" method="post" >
                    <span class="uk-form">
                        <div class="uk-margin-bottom">Имя <input style="width: 140px;" type="text" name="name" placeholder="Имя" value="<?=$user['name']?>"/></div>
                        <div class="uk-margin-bottom">E-mail <input style="width: 140px;" type="email" name="email" placeholder="E-mail" value="<?=$user['email']?>"/></div>
                        <div class="uk-margin-bottom">Телефон <input style="width: 140px;" type="text" name="phone" placeholder="Телефон" value="<?=$user['phone']?>" id="phone"/></div>
                        <script>
                            jQuery(function($){
                                $("#phone").mask("+7 (999) 999-99-99");
                            });
                        </script>
                        <div class="uk-margin-bottom">Пароль <input style="width: 140px;" type="password" name="password" placeholder="Пароль" value="<?=$user['password']?>"/></div>
                    </span>
                    <input type="submit" name="submit" class="uk-button uk-button-success" value="Изменить"><br>
                    <a href="/user/order/" class="uk-button uk-button-primary uk-margin-top">Мои заказы</a>
                </form>
            </div>
        <?endif;?>
    </div>
</div>
<?php include ROOT . '/templates/layouts/footer.php'; ?>