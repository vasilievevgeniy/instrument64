<?php

/** @var $ordersList[] */
/** @var $errors[] */
/** @var \Entities\Category[] $categories */

use Models\Order;

include ROOT . '/templates/layouts/header.php'; ?>

    <div class="uk-grid uk-grid-medium">
        <div class="uk-width-1-4">
            <img class="uk-margin-bottom" src="/templates/images/10.jpg">
            <?if(count($categories)):?>
                <div class="uk-panel uk-panel-box">
                    <h3 class="uk-panel-title">Категории</h3>
                    <ul class="uk-nav uk-nav-side">
                        <?foreach($categories as $category):?>
                            <?if($category->getActive()):?>
                                <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                            <?endif;?>
                        <?endforeach;?>
                    </ul>
                </div>
            <?endif;?>
            <img class="uk-margin-top" src="/templates/images/11.jpg">
        </div>
        <div class="uk-width-3-4">

            <ul class="uk-breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="uk-active"><span>Просмотр заказов</span></li>
            </ul>

            <h1 class="uk-panel-title">Просмотр заказов</h1>

            <?if(count($ordersList)):?>

                <table class="uk-table">
                    <thead>
                    <tr>
                        <th>ID заказа</th>
                        <th>Имя покупателя</th>
                        <th>Телефон покупателя</th>
                        <th>Дата оформления</th>
                        <th>Статус заказа</th>
                        <th>Управление заказом</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($ordersList as $order): ?>
                        <tr>
                            <td>
                                <a href="/admin/order/view/<?php echo $order['id']; ?>/">
                                    <?php echo $order['id']; ?>
                                </a>
                            </td>
                            <td><?php echo $order['user_name']; ?></td>
                            <td><?php echo $order['user_phone']; ?></td>
                            <td><?php echo $order['date']; ?></td>
                            <td><?php echo Order::getStatusText($order['status']); ?></td>
                            <td>
                                <div><a class="uk-button uk-button-primary uk-margin-small-bottom" href="/user/order/view/<?php echo $order['id']; ?>/" title="Смотреть">Просмотр</a></div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

            <?else:?>

                <div class="uk-alert uk-alert-danger">Нет заказов</div>

            <?endif;?>

        </div>
    </div>

<?php include ROOT . '/templates/layouts/footer.php'; ?>