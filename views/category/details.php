<?

use Components\DB;

/** @var int $id */
/** @var Entities\Category[] $categories */
/** @var \Entities\Category $currentCategory */
/** @var $brands[] */
/** @var $maxWeight int */
/** @var $minWeight int */
/** @var $minLength int */
/** @var $maxLength int */
/** @var $maxWidth int */
/** @var $minWidth int */
/** @var $minHeight int */
/** @var $maxHeight int */
/** @var $propertiesByCategory Entities\Property[] */

require_once ROOT . '/templates/layouts/header.php';?>

<div class="uk-grid uk-grid-medium uk-margin-bottom">
    <div class="uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-1">
        <img class="uk-margin-bottom" src="/templates/images/10.jpg">
        <?if(count($categories)):?>
            <div class="uk-panel-box uk-margin-bottom">
                <h3 class="uk-panel-title">Категории</h3>
                <ul class="uk-nav uk-nav-side">
                    <?foreach($categories as $category):?>
                        <?if($category->getActive()):?>
                            <li <?=($category->getID() == $id ? ' class="uk-active"' : '')?>><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                        <?endif;?>
                    <?endforeach;?>
                </ul>
            </div>
            <div class="uk-panel-box">

                <form method="post" class="filter">
                    <div class="uk-form">
                    <p>
                        <label for="amount-weight">Цена:</label><br>
                        <input name="price" type="text" id="amount-price">
                    </p>
                    <div id="slider-range-price"></div>

                    <p>
                        <div>Бренд:</div>
                        <?if(count($brands)):?>
                            <select name="brand[]" data-placeholder="Бренд" multiple="" class="chosen-select-brand" tabindex="-1">
                                <?foreach($brands as $brand):?>
                                    <option selected value="<?=$brand?>"><?=$brand?></option>
                                <?endforeach;?>
                            </select>
                            <script>
                                $(".chosen-select-brand").chosen({width: "100%"});
                            </script>
                        <?endif;?>
                    </p>

                    <p>
                        <label for="amount-weight">Вес (гр.):</label><br>
                        <input name="weight" type="text" id="amount-weight">
                    </p>
                    <div id="slider-range-weight"></div>

                    <p>
                        <label for="amount-weight">Длинна (мм.):</label><br>
                        <input name="length" type="text" id="amount-length">
                    </p>
                    <div id="slider-range-length"></div>

                    <p>
                        <label for="amount-width">Ширина (мм.):</label><br>
                        <input name="width" type="text" id="amount-width">
                    </p>
                    <div id="slider-range-width"></div>

                    <p>
                        <label for="amount-height">Высота (мм.):</label><br>
                        <input name="height" type="text" id="amount-height">
                    </p>
                    <div id="slider-range-height"></div>

                    <?if(count($propertiesByCategory)):?>
                        <?foreach($propertiesByCategory as $property):?>
                            <?switch ($property->getType()) {
                                case 'string':

                                    $dbh = DB::getConnection();
                                    $sql = "select * from property_value where id in (
                                        select property_value_id from ppp where type = 'string' and property_id = ? and product_id in (
                                            select id from product where category_id = ?
                                        )
                                    )";
                                    $query = $dbh->prepare($sql);
                                    $query->execute([$property->getID(), $id]);
                                    $props = [];
                                    while ($row = $query->fetch(\PDO::FETCH_OBJ)) {
                                        $props[$row->value] = $row;
                                    }

                                    ?><p>
                                        <div><?=$property->getLabel()?> <?=($property->getUnit() ? '(' . $property->getUnit() . ')' : '')?>:</div>
                                        <?if(count($props)):?>
                                            <select name="property_<?=$property->getName()?>[]" data-placeholder="<?=$property->getLabel()?>" multiple="" class="chosen-select-<?=$property->getName()?>" tabindex="-1">
                                                <?foreach($props as $prop):?>
                                                    <option selected value="<?=$prop->value?>"><?=$prop->value?></option>
                                                <?endforeach;?>
                                            </select>
                                            <script>
                                                $(".chosen-select-<?=$property->getName()?>").chosen({width: "100%"});
                                            </script>
                                        <?endif;?>
                                    </p><?

                                    break;
                                case 'integer':

                                    $dbh = DB::getConnection();
                                    $sql = "select * from property_value where id in (
                                        select property_value_id from ppp where type = 'integer' and property_id = ? and product_id in (
                                            select id from product where category_id = ?
                                        )
                                    )";
                                    $query = $dbh->prepare($sql);
                                    $query->execute([$property->getID(), $id]);
                                    $props = [];
                                    while ($row = $query->fetch(\PDO::FETCH_OBJ)) {
                                        $props[$row->id] = $row;
                                    }

                                    $maxProp = 0;
                                    $minProp = 0;
                                    $i = 1;
                                    foreach ($props as $prop) {
                                        if (!$maxProp) {
                                            $maxProp = $prop->value;
                                        } else {
                                            if ($maxProp < $prop->value) {
                                                $maxProp = $prop->value;
                                            }
                                        }
                                        if (!$minProp) {
                                            $minProp = $prop->value;
                                        } else {
                                            if ($minProp > $prop->value) {
                                                $minProp = $prop->value;
                                            }
                                        }
                                        $i++;
                                    }

                                    ?><p>
                                        <label for="amount-<?=$property->getName()?>"><?=$property->getLabel()?> <?=($property->getUnit() ? '(' . $property->getUnit() . ')' : '')?>:</label><br>
                                        <input name="property_<?=$property->getName()?>" type="text" id="amount-<?=$property->getName()?>">
                                    </p>
                                    <div id="slider-range-<?=$property->getName()?>"></div>

                                    <script>
                                        $( function() {
                                            $( "#slider-range-<?=$property->getName()?>" ).slider({
                                                range: true,
                                                min: <?=$minProp?>,
                                                max: <?=$maxProp?>,
                                                values: [ <?=$minProp?>, <?=$maxProp?> ],
                                                slide: function( event, ui ) {
                                                    $( "#amount-<?=$property->getName()?>" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
                                                }
                                            });
                                            $( "#amount-<?=$property->getName()?>" ).val( $( "#slider-range-<?=$property->getName()?>" ).slider( "values", 0 ) +
                                                " - " + $( "#slider-range-<?=$property->getName()?>" ).slider( "values", 1 ) );
                                        } );
                                    </script>

                                    <?

                                    break;
                                case 'boolean':

                                    ?><p>
                                        <div><?=$property->getLabel()?> <?=($property->getUnit() ? '(' . $property->getUnit() . ')' : '')?>:</div>
                                        <div>Все <input type="radio" checked name="property_<?=$property->getName()?>" value="all"></div>
                                        <div>Да <input type="radio" name="property_<?=$property->getName()?>" value="yes"></div>
                                        <div>Нет <input type="radio" name="property_<?=$property->getName()?>" value="no"></div>
                                    </p><?

                                    break;
                            }?>
                        <?endforeach;?>
                    <?endif;?>

                    </div>

                    <input class="uk-button-success uk-button uk-margin-top" type="submit" value="Показать" name="filter">

                </form>

            </div>
        <?endif;?>
        <img class="uk-margin-top" src="/templates/images/11.jpg">
    </div>
    <div class="uk-width-large-3-4 uk-width-medium-2-3 uk-width-small-1-1">

        <ul class="uk-breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><span>Категории</span></li>
            <li class="uk-active"><span><?=$currentCategory->getLabel()?></span></li>
        </ul>

        <h3 class="uk-panel-title">Товары</h3>
        <div class="uk-grid uk-grid-medium">
            <?require_once ROOT . '/templates/layouts/product_item.php'?>
        </div>
    </div>
</div>

<script>
    $( function() {
        $( "#slider-range-weight" ).slider({
            range: true,
            min: <?=$minWeight?>,
            max: <?=$maxWeight?>,
            values: [ <?=$minWeight?>, <?=$maxWeight?> ],
            slide: function( event, ui ) {
                $( "#amount-weight" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
            }
        });
        $( "#amount-weight" ).val( $( "#slider-range-weight" ).slider( "values", 0 ) +
            " - " + $( "#slider-range-weight" ).slider( "values", 1 ) );
    } );

    $( function() {
        $( "#slider-range-length" ).slider({
            range: true,
            min: <?=$minLength?>,
            max: <?=$maxLength?>,
            values: [ <?=$minLength?>, <?=$maxLength?> ],
            slide: function( event, ui ) {
                $( "#amount-length" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
            }
        });
        $( "#amount-length" ).val( $( "#slider-range-length" ).slider( "values", 0 ) +
            " - " + $( "#slider-range-length" ).slider( "values", 1 ) );
    } );

    $( function() {
        $( "#slider-range-width" ).slider({
            range: true,
            min: <?=$minWidth?>,
            max: <?=$maxWidth?>,
            values: [ <?=$minWidth?>, <?=$maxWidth?> ],
            slide: function( event, ui ) {
                $( "#amount-width" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
            }
        });
        $( "#amount-width" ).val( $( "#slider-range-width" ).slider( "values", 0 ) +
            " - " + $( "#slider-range-width" ).slider( "values", 1 ) );
    } );

    $( function() {
        $( "#slider-range-height" ).slider({
            range: true,
            min: <?=$minHeight?>,
            max: <?=$maxHeight?>,
            values: [ <?=$minHeight?>, <?=$maxHeight?> ],
            slide: function( event, ui ) {
                $( "#amount-height" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
            }
        });
        $( "#amount-height" ).val( $( "#slider-range-height" ).slider( "values", 0 ) +
            " - " + $( "#slider-range-height" ).slider( "values", 1 ) );
    } );

    $( function() {
        $( "#slider-range-price" ).slider({
            range: true,
            min: <?=$minPrice?>,
            max: <?=$maxPrice?>,
            values: [ <?=$minPrice?>, <?=$maxPrice?> ],
            slide: function( event, ui ) {
                $( "#amount-price" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
            }
        });
        $( "#amount-price" ).val( $( "#slider-range-price" ).slider( "values", 0 ) +
            " - " + $( "#slider-range-price" ).slider( "values", 1 ) );
    } );
</script>

<?require_once ROOT . '/templates/layouts/footer.php';?>