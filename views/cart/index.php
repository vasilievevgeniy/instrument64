<?

/** @var Entities\Category[] $categories */
/** @var $productsInCart[] */
/** @var $products[] */
/** @var $totalPrice int */

include ROOT . '/templates/layouts/header.php'; ?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <img class="uk-margin-bottom" src="/templates/images/10.jpg">
        <?if(count($categories)):?>
            <div class="uk-panel-box">
                <h3 class="uk-panel-title">Категории</h3>
                <ul class="uk-nav uk-nav-side">
                    <?foreach($categories as $category):?>
                        <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                    <?endforeach;?>
                </ul>
            </div>
        <?endif;?>
        <img class="uk-margin-top" src="/templates/images/11.jpg">
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="uk-active"><span>Корзина</span></li>
        </ul>

        <h3 class="uk-panel-title">Корзина</h3>

        <?$messages = [];?>
        <?if($productsInCart):?>
            <?foreach($products as $product):?>
                <?
                $prod = Models\Product::getProductByID($product['id']);
                if ($productsInCart[$product['id']] > $prod->getCount()) {
                    $_SESSION['products'][$product['id']] = $prod->getCount();
                    $messages[] = 'Количество товара "' . $product['label'] . '" в корзине (' . $productsInCart[$product['id']] . ' шт.) больше, чем есть на складе (' . $prod->getCount() . ' шт.).';
                }
                ?>
            <?endforeach;?>
        <?endif;?>
        <?if(count($messages)):?>
            <div class="uk-alert uk-alert-danger" data-uk-alert="">
                <a href="" class="uk-alert-close uk-close"></a>
                <p>
                    <?foreach($messages as $message):?>
                        <?=$message?><br>
                    <?endforeach;?>
                    Количество товара изменено.
                    <?$cnt = array_sum($_SESSION['products']);?>
                    <script>
                        $(document).ready(function () {
                            $('#cart-count').text('<?=$cnt?>');
                        });
                    </script>
                </p>
            </div>
        <?endif;?>

        <?if($productsInCart):?>
            <p>Выбранные товары:</p>
            <table class="uk-table">
                <thead>
                    <tr>
                        <th>Код товара</th>
                        <th>Название</th>
                        <th>Стоимость</th>
                        <th>Количество</th>
                        <th>Удалить</th>
                    </tr>
                </thead>
                <?foreach($products as $product):?>
                    <tbody>
                        <tr>
                            <td><?=$product['name']?></td>
                            <td><a href="/product/<?=$product['id']?>/"><?=$product['label']?></a></td>
                            <td><?=number_format($product['price'], 0, '', ' ')?> <i class="uk-icon-rub"></i></td>

                            <?
                            $prod = Models\Product::getProductByID($product['id']);
                            if ($productsInCart[$product['id']] > $prod->getCount()) {
                                $productsInCart[$product['id']] = $prod->getCount();
                            }
                            ?>

                            <td>
                                <input type="number" id="<?=$product['id']?>" style="width: 40px; margin-top: -3px" min="0" class="product-count" value="<?=$productsInCart[$product['id']]?>">
                            </td>
                            <td><a style="color: red;" href="/cart/delete/<?=$product['id']?>/"><i class="fa fa-times uk-icon-times"></i></a></td>
                        </tr>
                    </tbody>
                <?endforeach;?>
                <tr>
                    <td>Общая стоимость:</td>
                    <td><?=number_format($totalPrice, 0, '', ' ')?> <i class="uk-icon-rub"></i></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <script>
                $(document).on('change', '.product-count', function() {
                    var productID = $(this).attr('id');
                    var productCount = $(this).val();
                    if (productID && productCount) {
                        $.ajax({
                            type: "POST",
                            url: "/ajax/product-count.php",
                            data: "productID=" + productID + "&productCount=" + productCount,
                            success: function(msg){
                                if (msg) {
                                    var a = msg.split('/');
                                    $('#' + a[0]).val(a[1]);
                                }
                            }
                        });
                    }
                })
            </script>
            <p><a class="btn btn-default checkout uk-button uk-button-success" href="/cart/checkout/"><i class="fa fa-shopping-cart"></i> Оформить заказ</a></p>
        <?php else: ?>
            <p>Корзина пуста</p>
            <a class="btn btn-default checkout uk-button uk-button-primary" href="/"><i class="fa fa-shopping-cart"></i> Вернуться к покупкам</a>
        <?php endif; ?>
    </div>
</div>

<?php include ROOT . '/templates/layouts/footer.php'; ?>