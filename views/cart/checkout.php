<?

/** @var Entities\Category[] $categories */
/** @var $totalQuantity int */
/** @var $totalPrice int */
/** @var $userName string */
/** @var $userPhone string */
/** @var $userComment string */

include ROOT . '/templates/layouts/header.php'?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <img class="uk-margin-bottom" src="/templates/images/10.jpg">
        <?if(count($categories)):?>
            <div class="uk-panel-box">
                <h3 class="uk-panel-title">Категории</h3>
                <ul class="uk-nav uk-nav-side">
                    <?foreach($categories as $category):?>
                        <li><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                    <?endforeach;?>
                </ul>
            </div>
        <?endif;?>
        <img class="uk-margin-top" src="/templates/images/11.jpg">
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><a href="/cart/">Корзина</a></li>
            <li class="uk-active"><span>Оформление заказа</span></li>
        </ul>

        <h3 class="uk-panel-title">Оформление заказа</h3>

        <?if($result):?>
            <p>Заказ оформлен. Мы Вам перезвоним.</p>
        <?else:?>

            <p>Выбрано товаров: <?=$totalQuantity?>, на сумму: <?=number_format($totalPrice, 0, '', ' ')?> <i class="uk-icon-rub"></i>.</p>

            <?if(!$result):?>
                <div>
                    <p>Для оформления заказа заполните форму. Наш менеджер свяжется с Вами.</p>
                    <?if(isset($errors) && is_array($errors)):?>
                        <?foreach($errors as $error):?>
                            <div class="uk-alert uk-alert-danger"><?=$error?></div>
                        <?endforeach;?>
                    <?endif;?>
                    <div>
                        <form action="" method="post">
                            <div class="uk-form">

                                <div class="uk-form-row">
                                    <label class="uk-form-label">Ваше имя</label>
                                    <div class="uk-form-controls"><input type="text" name="userName" placeholder="" value="<?=$userName?>"></div>
                                </div>

                                <div class="uk-form-row">
                                    <label>Номер телефона</label>
                                    <div class="uk-form-controls"><input type="text" name="userPhone" placeholder="" value="<?=$userPhone?>"></div>
                                </div>

                                <div class="uk-form-row">
                                    <label>Комментарий к заказу</label>
                                    <div class="uk-form-controls"><input type="text" name="userComment" placeholder="Сообщение" value="<?=$userComment?>"></div>
                                </div>

                            </div>
                            <div class="uk-margin-top"><input type="submit" name="submit" class="btn btn-default uk-button uk-button-success" value="Оформить"></div>
                        </form>
                    </div>
                </div>
            <?endif;?>
        <?endif;?>

    </div>
</div>

<?php include ROOT . '/templates/layouts/footer.php'; ?>