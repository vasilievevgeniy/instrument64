<?php

/** @var Entities\Category[] $categories */
/** @var Entities\Property[] $properties */
/** @var $errors[] */

require_once ROOT . '/templates/layouts/admin_header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/category/">Управление категориями</a></li>
                <li><a href="/admin/property/">Управление свойствами</a></li>
                <li><a href="/admin/product/">Управление продуктами</a></li>
                <li><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li class="uk-active"><span>Администратор</span></li>
        </ul>

    </div>
</div>

<?require_once ROOT . '/templates/layouts/admin_footer.php';?>