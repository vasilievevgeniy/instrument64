<?php

/** @var Entities\Property $property */
/** @var Entities\Category[] $categories */
/** @var $categoryIDs[] */
/** @var $errors[] */

require_once ROOT . '/templates/layouts/admin_header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/category/">Управление категориями</a></li>
                <li class="uk-active"><a href="/admin/property/">Управление свойствами</a></li>
                <li><a href="/admin/product/">Управление продуктами</a></li>
                <li><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">
        <ul class="uk-breadcrumb">
            <li><a href="/admin/">Администратор</a></li>
            <li><a href="/admin/property/">Управление свойствами</a></li>
            <li class="uk-active"><span>Добавить свойство</span></li>
        </ul>
        <h1 class="uk-panel-title">Добавить свойство</h1>
        <?foreach($errors as $error):?>
            <div class="uk-alert uk-alert-danger"><?=$error?></div>
        <?endforeach;?>
        <?if($property):?>
            <form method="post">
                <div class="uk-form uk-form-stacked uk-margin-bottom">
                    <div class="uk-form-row">
                        <label class="uk-form-label">Активность</label>
                        <div class="uk-form-controls">
                            <input type="checkbox" name="active" <?=($property->getActive() ? 'checked' : '')?>>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Название *</label>
                        <div class="uk-form-controls">
                            <input type="text" name="label" placeholder="Название" value="<?=$property->getLabel()?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Код *</label>
                        <div class="uk-form-controls">
                            <input type="text" name="name" placeholder="Код" value="<?=$property->getName()?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Категории</label>
                        <div class="uk-form-controls">
                            <select name="category_ids[]" data-placeholder="Категории" multiple="" class="chosen-select-category" tabindex="-1">
                                <option value="0" selected>Нет</option>
                                <?foreach($categories as $category):?>
                                    <?if($category->getActive()):?>
                                        <option value="<?=$category->getID()?>" <?=(in_array($category->getID(), $categoryIDs) ? ' selected' : '')?>><?=$category->getLabel()?></option>
                                    <?endif;?>
                                <?endforeach;?>
                            </select>
                            <script>
                                $(".chosen-select-category").chosen({width: "184px"});
                            </script>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Тип</label>
                        <div class="uk-form-controls">
                            <select name="type">
                                <option value="integer"<?=($property->getType() === 'integer' ? ' selected' : '')?>>Число</option>
                                <option value="string"<?=($property->getType() === 'string' ? ' selected' : '')?>>Строка</option>
                                <option value="boolean"<?=($property->getType() === 'boolean' ? ' selected' : '')?>>Булев</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Единица измерения</label>
                        <div class="uk-form-controls">
                            <input type="text" name="unit" placeholder="Единица измерения" value="<?=$property->getUnit()?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Порядок</label>
                        <div class="uk-form-controls">
                            <input type="text" name="sort" placeholder="Порядок" value="<?=$property->getSort()?>">
                        </div>
                    </div>
                </div>
                <input class="uk-button uk-button-success" type="submit" name="create_property_submit" value="Создать">
            </form>
        <?endif;?>
    </div>
</div>

<?require_once ROOT . '/templates/layouts/admin_footer.php';?>