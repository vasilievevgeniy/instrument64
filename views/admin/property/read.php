<?php

/** @var \Entities\Property[] $properties */
/** @var $errors[] */

require_once ROOT . '/templates/layouts/admin_header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/category/">Управление категориями</a></li>
                <li class="uk-active"><a href="/admin/property/">Управление свойствами</a></li>
                <li><a href="/admin/product/">Управление продуктами</a></li>
                <li><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/admin/">Администратор</a></li>
            <li class="uk-active"><span>Управление свойствами</span></li>
        </ul>

        <h1 class="uk-panel-title">Просмотр свойств</h1>

        <?foreach($errors as $error):?>
            <div class="uk-alert uk-alert-danger"><?=$error?></div>
        <?endforeach;?>

        <?if(count($properties)):?>
            <ul class="uk-list">
                <?foreach($properties as $property):?>
                    <li class="uk-margin-small-bottom">
                        <span><?=$property->getLabel()?></span>
                        <a class="uk-button uk-button-success" href="/admin/property/update/<?=$property->getID()?>/">Изменить</a>
                        <a class="uk-button uk-button-danger" href="/admin/property/delete/<?=$property->getID()?>/">Удалить</a>
                    </li>
                <?endforeach;?>
            </ul>
        <?endif;?>
        <a class="uk-button uk-button-primary" href="/admin/property/create/">Создать свойство</a>
    </div>
</div>

<?require_once ROOT . '/templates/layouts/admin_footer.php';?>
