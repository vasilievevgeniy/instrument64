<?php

/** @var Entities\Product $product */
/** @var Entities\Category[] $categories */

/** @var Entities\Property[] $properties */
/** @var Entities\PropertyValue[] $propertyValues */
/** @var Entities\RelationshipPropertyAndPropertyValue[] $relationshipPropertiesAndPropertyValues */

/** @var $errors[] */

require_once ROOT . '/templates/layouts/admin_header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/category/">Управление категориями</a></li>
                <li><a href="/admin/property/">Управление свойствами</a></li>
                <li class="uk-active"><a href="/admin/product/">Управление продуктами</a></li>
                <li><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">
        <ul class="uk-breadcrumb">
            <li><a href="/admin/">Администратор</a></li>
            <li><a href="/admin/product/">Управление продуктами</a></li>
            <li class="uk-active"><span>Добавить продукт</span></li>
        </ul>
        <h1 class="uk-panel-title">Добавить продукт</h1>
        <?foreach($errors as $error):?>
            <div class="uk-alert uk-alert-danger"><?=$error?></div>
        <?endforeach;?>
        <?if($product):?>
            <form method="post" enctype="multipart/form-data">
                <div class="uk-form uk-form-stacked uk-margin-bottom">
                    <div class="uk-grid uk-grid-small">
                        <div class="uk-width-1-3">
                            <div class="uk-form-row">
                                <label class="uk-form-label">Активность</label>
                                <div class="uk-form-controls">
                                    <input type="checkbox" name="active" <?=($product->getActive() ? 'checked' : '')?>>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Название *</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="label" placeholder="Название" value="<?=$product->getLabel()?>">
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Код *</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="name" placeholder="Код" value="<?=$product->getName()?>">
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Категория</label>
                                <div class="uk-form-controls">
                                    <select name="category_id" data-placeholder="Категории" class="chosen-select-category" tabindex="-1">
                                        <option value="0">Нет</option>
                                        <?foreach($categories as $category):?>
                                            <option value="<?=$category->getID()?>"<?=($category->getID() == $product->getCategoryID()) ? ' selected' : ''?>><?=$category->getLabel()?></option>
                                        <?endforeach;?>
                                    </select>
                                    <script>
                                        $(".chosen-select-category").chosen({width: "184px"});
                                    </script>
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Порядок</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="sort" placeholder="Порядок" value="<?=$product->getSort()?>">
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Цена</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="price" placeholder="Цена" value="<?=$product->getPrice()?>">
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Брэнд</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="brand" placeholder="Брэнд" value="<?=$product->getBrand()?>">
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Количество</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="count" placeholder="Количество" value="<?=$product->getCount()?>">
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="uk-form-row">
                                <label class="uk-form-label">Вес</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="weight" placeholder="Вес" value="<?=$product->getWidth()?>"> гр.
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Длина</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="length" placeholder="Длина" value="<?=$product->getLength()?>"> мм.
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Ширина</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="width" placeholder="Ширина" value="<?=$product->getWidth()?>"> мм.
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Высота</label>
                                <div class="uk-form-controls">
                                    <input type="text" name="height" placeholder="Высота" value="<?=$product->getHeight()?>"> мм.
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Изображение</label>
                                <div class="uk-form-controls">
                                    <input type="file" name="image" value="<?=$product->getImage()?>">
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Краткое описание</label>
                                <div class="uk-form-controls">
                                    <input placeholder="Краткое описание" type="text" name="short_description" value="<?=$product->getShortDescription()?>">
                                </div>
                            </div>
                            <div class="uk-form-row">
                                <label class="uk-form-label">Полное описание</label>
                                <div class="uk-form-controls">
                                    <textarea name="full_description"><?=$product->getFullDescription()?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="properties">
                                <?if(count($properties)):?>
                                    <?foreach($properties as $property):?>

                                        <?$value = null;
                                        if (count($relationshipPropertiesAndPropertyValues)) {
                                            foreach ($relationshipPropertiesAndPropertyValues as $relationship) {
                                                if ($property->getID() == $relationship->getPropertyID()) {

                                                    if (count($propertyValues)) {
                                                        foreach ($propertyValues as $propertyValue) {
                                                            if ($relationship->getPropertyValueID() == $propertyValue->getID()) {

                                                                $value = $propertyValue->getValue();

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }?>

                                        <?switch ($property->getType()) {
                                            case 'string':
                                                ?>
                                                <div>
                                                    <label>
                                                        <?=$property->getLabel()?>
                                                        <input type="text" name="property_<?=$property->getName()?>" placeholder="<?=$property->getLabel()?>" value="<?=($value ? $value : '')?>">
                                                    </label>
                                                    <?=$property->getUnit()?>
                                                </div>
                                                <?
                                                break;
                                            case 'integer':
                                                ?>
                                                <div>
                                                    <label>
                                                        <?=$property->getLabel()?>
                                                        <input type="text" name="property_<?=$property->getName()?>" placeholder="<?=$property->getLabel()?>" value="<?=($value ? $value : '')?>">
                                                    </label>
                                                    <?=$property->getUnit()?>
                                                </div>
                                                <?
                                                break;
                                            case 'boolean':
                                                ?>
                                                <div>
                                                    <label>
                                                        <?=$property->getLabel()?>
                                                    </label>
                                                    <div>Нет <input <?=($value == '0' ? ' checked' : '')?> type="radio" name="property_<?=$property->getName()?>" value="0"></div>
                                                    <div>Да <input <?=($value == '1' ? ' checked' : '')?> type="radio" name="property_<?=$property->getName()?>" value="1"></div>
                                                    <?=$property->getUnit()?>
                                                </div>
                                                <?
                                                break;
                                        }?>
                                    <?endforeach;?>
                                <?endif;?>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="submit" class="uk-button uk-button-success" name="create_product_submit" value="Создать">
            </form>
        <?endif;?>
    </div>
</div>

<?require_once ROOT . '/templates/layouts/admin_footer.php';?>