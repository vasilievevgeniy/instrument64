<?php

/** @var array $fullProducts */
/** @var $errors[] */

require_once ROOT . '/templates/layouts/admin_header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li><a href="/admin/category/">Управление категориями</a></li>
                <li><a href="/admin/property/">Управление свойствами</a></li>
                <li class="uk-active"><a href="/admin/product/">Управление продуктами</a></li>
                <li><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/admin/">Администратор</a></li>
            <li class="uk-active"><span>Управление продуктами</span></li>
        </ul>

        <h1 class="uk-panel-title">Просмотр продуктов</h1>

        <?foreach($errors as $error):?>
            <div class="uk-alert uk-alert-danger"><?=$error?></div>
        <?endforeach;?>

        <?if(count($fullProducts)):?>
            <?foreach($fullProducts as $categoryID => $products):?>
                <?$cat = \Models\Category::getCategoryByID($categoryID);?>
                <h4><b><?=$cat->getLabel()?></b></h4>
                <?if(count($products)):?>
                    <ul class="uk-list">
                        <?
                        /** @var \Entities\Product $product */
                        foreach($products as $product):?>
                            <li class="uk-margin-small-bottom">
                                <span><?=$product->getLabel()?></span>
                                <a class="uk-button uk-button-success" href="/admin/product/update/<?=$product->getID()?>/">Изменить</a>
                                <a class="uk-button uk-button-danger" href="/admin/product/delete/<?=$product->getID()?>/">Удалить</a>
                            </li>
                        <?endforeach;?>
                    </ul>
                <?endif;?>
            <?endforeach;?>
        <?endif;?>
        <a class="uk-button uk-button-primary" href="/admin/product/create/">Создать продукт</a>
    </div>
</div>

<?require_once ROOT . '/templates/layouts/admin_footer.php';?>