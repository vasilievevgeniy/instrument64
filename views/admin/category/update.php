<?php

/** @var Entities\Category $category */
/** @var $errors[] */

require_once ROOT . '/templates/layouts/admin_header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li class="uk-active"><a href="/admin/category/">Управление категориями</a></li>
                <li><a href="/admin/property/">Управление свойствами</a></li>
                <li><a href="/admin/product/">Управление продуктами</a></li>
                <li><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">
        <ul class="uk-breadcrumb">
            <li><a href="/admin/">Администратор</a></li>
            <li><a href="/admin/category/">Управление категориями</a></li>
            <li class="uk-active"><span>Изменение категории</span></li>
        </ul>
        <h1 class="uk-panel-title">Изменить категорию</h1>
        <?foreach($errors as $error):?>
            <div class="uk-alert uk-alert-danger"><?=$error?></div>
        <?endforeach;?>
        <?if($category):?>
            <form method="post">
                <div class="uk-form uk-form-stacked uk-margin-bottom">
                    <div class="uk-form-row">
                        <label class="uk-form-label">Активность</label>
                        <div class="uk-form-controls">
                            <input type="checkbox" name="active" <?=($category->getActive() ? 'checked' : '')?>>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Название *</label>
                        <div class="uk-form-controls">
                            <input type="text" name="label" placeholder="Название" value="<?=$category->getLabel()?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Код *</label>
                        <div class="uk-form-controls">
                            <input type="text" name="name" placeholder="Код" value="<?=$category->getName()?>">
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Порядок</label>
                        <div class="uk-form-controls">
                            <input type="text" name="sort" placeholder="Порядок" value="<?=$category->getSort()?>">
                        </div>
                    </div>
                </div>
                <input class="uk-button uk-button-success" type="submit" name="update_category_submit" value="Изменить">
            </form>
        <?endif;?>
    </div>
</div>

<?require_once ROOT . '/templates/layouts/admin_footer.php';?>
