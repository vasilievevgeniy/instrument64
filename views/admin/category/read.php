<?php

/** @var Entities\Category[] $categories */
/** @var $errors[] */

require_once ROOT . '/templates/layouts/admin_header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <div class="uk-panel-box">
            <h3 class="uk-panel-title">Ресурсы</h3>
            <ul class="uk-nav uk-nav-side">
                <li class="uk-active"><a href="/admin/category/">Управление категориями</a></li>
                <li><a href="/admin/property/">Управление свойствами</a></li>
                <li><a href="/admin/product/">Управление продуктами</a></li>
                <li><a href="/admin/order/">Управление заказами</a></li>
            </ul>
        </div>
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/admin/">Администратор</a></li>
            <li class="uk-active"><span>Управление категориями</span></li>
        </ul>

        <h1 class="uk-panel-title">Просмотр категорий</h1>

        <?foreach($errors as $error):?>
            <div class="uk-alert uk-alert-danger"><?=$error?></div>
        <?endforeach;?>

        <?if(count($categories)):?>
            <ul class="uk-list">
                <?foreach($categories as $category):?>
                    <li class="uk-margin-small-bottom">
                        <span><?=$category->getLabel()?></span>
                        <a class="uk-button uk-button-success" href="/admin/category/update/<?=$category->getID()?>/">Изменить</a>
                        <a class="uk-button uk-button-danger" href="/admin/category/delete/<?=$category->getID()?>/">Удалить</a>
                    </li>
                <?endforeach;?>
            </ul>
        <?endif;?>

        <a class="uk-button uk-button-primary" href="/admin/category/create/">Создать категорию</a>
    </div>
</div>

<?require_once ROOT . '/templates/layouts/admin_footer.php';?>
