<?

/** @var Entities\Product $product */
/** @var Entities\Category[] $categories */
/** @var Entities\Property[] $properties */
/** @var Entities\PropertyValue[] $propertyValues */
/** @var Entities\RelationshipPropertyAndPropertyValue[] $relationshipPropertiesAndPropertyValues */

require_once ROOT . '/templates/layouts/header.php';?>

<div class="uk-grid uk-grid-medium">
    <div class="uk-width-1-4">
        <img class="uk-margin-bottom" src="/templates/images/10.jpg">
        <?if(count($categories)):?>
            <div class="uk-panel uk-panel-box">
                <h3 class="uk-panel-title">Категории</h3>
                <ul class="uk-nav uk-nav-side">
                    <?foreach($categories as $category):?>
                        <?if($category->getActive()):?>
                            <li <?=($product->getCategoryID() == $category->getID() ? ' class="uk-active"' : '')?>><a href="/category/<?=$category->getID()?>/"><?=$category->getLabel()?></a></li>
                        <?endif;?>
                    <?endforeach;?>
                </ul>
            </div>
        <?endif;?>
        <img class="uk-margin-top" src="/templates/images/11.jpg">
    </div>
    <div class="uk-width-3-4">

        <ul class="uk-breadcrumb">
            <li><a href="/">Главная</a></li>
            <li><span>Продукты</span></li>
            <li class="uk-active"><span><?=$product->getLabel()?></span></li>
        </ul>

        <?if($product->getActive()):?>

            <h1 class="uk-panel-title"><?=$product->getLabel()?></h1>

            <div>
                <?if($product->getImage()):?>
                    <a class="uk-margin-right" href="/upload/<?=$product->getImage()?>" data-uk-lightbox title="" style="float: left; max-width: 50%;">
                        <img class="uk-margin-right uk-margin-bottom" src="/upload/<?=$product->getImage()?>">
                    </a>
                <?endif;?>

                <?if($product->getPrice()):?>
                    <div class="price-font uk-margin-bottom uk-margin-top"><b>Цена:</b> <?=number_format($product->getPrice(), 0, '', ' ')?> <i class="uk-icon-rub"></i></div>
                <?endif;?>

                <a href="" class="btn btn-default add-to-cart uk-button uk-button-success uk-margin-small-bottom uk-margin-small-top" data-id="<?=$product->getID()?>"><i class="uk-icon-cart-plus"></i> В корзину</a>

            </div>

            <div style="clear: both;">
                <?if($product->getBrand()):?>
                    <div><b>Бренд:</b> <?=$product->getBrand()?></div>
                <?endif;?>
                <?if($product->getWeight()):?>
                    <div><b>Вес:</b> <?=$product->getWeight()?> гр.</div>
                <?endif;?>
                <?if($product->getLength()):?>
                    <div><b>Длина:</b> <?=$product->getLength()?> мм.</div>
                <?endif;?>
                <?if($product->getWidth()):?>
                    <div><b>Ширина:</b> <?=$product->getWidth()?> мм.</div>
                <?endif;?>
                <?if($product->getHeight()):?>
                    <div><b>Высота:</b> <?=$product->getHeight()?> мм.</div>
                <?endif;?>

                <?if(count($properties)):?>
                    <?foreach($properties as $property):?>

                        <?$value = null;
                        if (count($relationshipPropertiesAndPropertyValues)) {
                            foreach ($relationshipPropertiesAndPropertyValues as $relationship) {
                                if ($property->getID() == $relationship->getPropertyID()) {

                                    if (count($propertyValues)) {
                                        foreach ($propertyValues as $propertyValue) {
                                            if ($relationship->getPropertyValueID() == $propertyValue->getID()) {

                                                $value = $propertyValue->getValue() . ' ' . $property->getUnit();
                                            }
                                        }
                                    }
                                }
                            }
                        }?>

                        <?switch ($property->getType()) {
                            case 'string':
                                ?><div><b><?=$property->getLabel()?></b>: <?=$value?></div><?
                                break;
                            case 'integer':
                                ?><div><b><?=$property->getLabel()?></b>: <?=$value?></div><?
                                break;
                            case 'boolean':
                                ?><div><b><?=$property->getLabel()?></b>: <?=($value ? 'Да' : 'Нет')?></div><?
                                break;
                        }?>
                    <?endforeach;?>
                <?endif;?>

                <?if($product->getCount()):?>
                    <div><b>Количество на складе:</b> <?=$product->getCount()?> шт.</div>
                <?endif;?>

                <?if($product->getFullDescription()):?>
                    <br><div><b>Полное описание:</b> <?=$product->getFullDescription()?></div>
                <?endif;?>
            </div>

        <?endif;?>

    </div>
</div>

<?require_once ROOT . '/templates/layouts/footer.php';?>