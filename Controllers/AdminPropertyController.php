<?php

use Models\Property;
use Models\Category;
use Components\Validate;
use Components\Admin;

class AdminPropertyController extends Admin
{
    public function actionCreate()
    {
        self::checkAdmin();

        $errors = [];
        $categories = Category::getCategories();
        $categoryIDs = [];

        $property = new Entities\Property();
        if ($_POST['create_property_submit']) {
            $property->setLabel($_POST['label']);
            $property->setName($_POST['name']);
            $property->setSort($_POST['sort']);
            $property->setActive($_POST['active'] == 'on' ? 1 : 0);
            $property->setType($_POST['type']);
            $property->setUnit($_POST['unit']);

            if (count($_POST['category_ids'])) {
                $categoryIDs = $_POST['category_ids'];
                if (intval($categoryIDs[0]) === 0) {
                    $categoryIDs[0] = null;
                }
            }

            $errors = array_merge($errors, Validate::checkLabel($property->getLabel()));
            $errors = array_merge($errors, Validate::checkName($property->getName()));
            $errors = array_merge($errors, Validate::checkSort($property->getSort()));
            $errors = array_merge($errors, Validate::checkCategoryIDs($categoryIDs));
            $errors = array_merge($errors, Validate::checkType($property->getType()));

            if (!count($errors)) {
                if (Property::create($property, $categoryIDs)) {
                    header('Location: /admin/property/');
                } else {
                    $errors['create'] = 'Свойство не было добавлено';
                }
            }
        }
        require_once ROOT . '/views/admin/property/create.php';
        return true;
    }

    public function actionRead()
    {
        self::checkAdmin();

        $errors = [];
        $properties = Property::getProperties();
        if (!count($properties)) {
            $errors['read'] = 'Нет свойств';
        }
        require_once ROOT . '/views/admin/property/read.php';
        return true;
    }

    public function actionUpdate($id)
    {
        self::checkAdmin();

        if (intval($id)) {
            $errors = [];
            $categories = Category::getCategories();
            $categoryIDs = [];
            /** @var Entities\Property $property */
            $property = Property::getPropertyByID($id);
            if ($property) {
                $categoriesByPropertyID = Category::getCategoriesByPropertyID($property->getID());
                /** @var \Entities\Entity $category */
                foreach ($categoriesByPropertyID as $category) {
                    $categoryIDs[$category->getID()] = $category->getID();
                }
                if ($_POST['update_property_submit']) {
                    $property->setID(intval($id));
                    $property->setLabel($_POST['label']);
                    $property->setName($_POST['name']);
                    $property->setSort($_POST['sort']);
                    $property->setActive($_POST['active'] == 'on' ? 1 : 0);
                    $property->setType($_POST['type']);
                    $property->setUnit($_POST['unit']);
                    if (count($_POST['category_ids'])) {
                        $categoryIDs = $_POST['category_ids'];
                        if (intval($categoryIDs[0]) === 0) {
                            $categoryIDs[0] = null;
                        }
                    }
                    $errors = array_merge($errors, Validate::checkLabel($property->getLabel()));
                    $errors = array_merge($errors, Validate::checkName($property->getName()));
                    $errors = array_merge($errors, Validate::checkSort($property->getSort()));
                    $errors = array_merge($errors, Validate::checkCategoryIDs($categoryIDs));
                    $errors = array_merge($errors, Validate::checkType($property->getType()));
                    if (!count($errors)) {
                        if (Property::update($property, $categoryIDs)) {
                            header('Location: /admin/property/');
                        } else {
                            $errors['update'] = 'Свойство не было изменено';
                        }
                    }
                }
            } else {
                $errors['update'] = 'Свойство не найдено';
            }
            require_once ROOT . '/views/admin/property/update.php';
            return true;
        }
        return false;
    }

    public function actionDelete($id)
    {
        self::checkAdmin();

        if (intval($id)) {
            $errors = [];
            $property = Property::getPropertyByID($id);
            if ($property) {
                if ($_POST['delete_property_submit']) {
                    if (Property::delete($id)) {
                        header('Location: /admin/property/');
                    } else {
                        $errors['delete'] = 'Свойство не было удалено';
                    }
                }
            } else {
                $errors['delete'] = 'Свойство не было найдено';
            }
            require_once ROOT . '/views/admin/property/delete.php';
            return true;
        }
        return false;
    }
}
