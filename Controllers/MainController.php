<?php

use Models\Category;
use Models\Product;

class MainController
{
    public function actionMain()
    {
        $categories = Category::getCategories();
        $products = Product::getProductsCount(9);

        require_once ROOT . '/views/main/main.php';

        return true;
    }

    public function actionAbout()
    {
        $categories = Category::getCategories();

        require_once ROOT . '/views/main/about.php';

        return true;
    }

    public function actionFeedback()
    {
        $categories = Category::getCategories();

        $errors = [];
        $send = false;

        if ($_POST['feedback']) {
            $name = $_POST['name'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $message = $_POST['message'];

            if (!$name) {
                $errors[] = 'Введите имя';
            }
            if (!$phone) {
                $errors[] = 'Введите телефон';
            }
            if (!$email) {
                $errors[] = 'Введите e-mail';
            }
            if (!$message) {
                $errors[] = 'Введите сообщение';
            }

            if (!count($errors)) {
                $send = true;

                $fullMessage = "$name $phone $email $message";
                mail('admin@instrument64.ru', 'Feedback', $fullMessage);
            }
        }

        require_once ROOT . '/views/main/feedback.php';

        return true;
    }

    public function actionContacts()
    {
        $categories = Category::getCategories();

        require_once ROOT . '/views/main/contacts.php';

        return true;
    }
}
