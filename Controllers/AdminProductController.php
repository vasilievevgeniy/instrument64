<?php

use Models\Product;
use Models\Category;
use Models\Property;
use Models\PropertyValue;
use Models\RelationshipPropertyAndPropertyValue;
use Components\Validate;
use Components\Admin;

class AdminProductController extends Admin
{
    public function actionCreate()
    {
        self::checkAdmin();

        $errors = [];
        $categories = Category::getCategories();

        $properties = [];
        $propertyValues = [];
        $propertyAndPropertyValues = [];
        $relationshipPropertiesAndPropertyValues = [];

        $product = new Entities\Product();

        /** @var \Entities\RelationshipPropertyAndPropertyValue $relationshipPropertiesAndPropertyValues */
        $relationshipPropertiesAndPropertyValues = RelationshipPropertyAndPropertyValue::getRelationshipPropertiesAndPropertyValuesByProduct($product);
        /** @var \Entities\Property[] $properties */
        $properties = Property::getPropertiesByCategoryID($product->getCategoryID());
        /** @var \Entities\PropertyValue $propertyValues[] */
        $propertyValues = PropertyValue::getPropertyValuesByPropertiesAndProduct($properties, $product);

        if ($_POST['create_product_submit']) {
            $product->setLabel($_POST['label']);
            $product->setName($_POST['name']);
            $product->setSort($_POST['sort']);
            $product->setActive($_POST['active'] == 'on' ? 1 : 0);
            $product->setCategoryID(intval($_POST['category_id']) === 0 ? null : intval($_POST['category_id']));
            $product->setBrand($_POST['brand']);
            $product->setPrice($_POST['price']);
            $product->setWeight($_POST['weight']);
            $product->setLength($_POST['length']);
            $product->setWidth($_POST['width']);
            $product->setHeight($_POST['height']);
            $product->setImage($_FILES['image']);
            $product->setCount($_POST['count']);
            $product->setShortDescription($_POST['short_description']);
            $product->setFullDescription($_POST['full_description']);

            $errors = array_merge($errors, Validate::checkLabel($product->getLabel()));
            $errors = array_merge($errors, Validate::checkName($product->getName()));
            $errors = array_merge($errors, Validate::checkSort($product->getSort()));
            $errors = array_merge($errors, Validate::checkCategoryID($product->getCategoryID()));
            $errors = array_merge($errors, Validate::checkPrice($product->getPrice()));
            $errors = array_merge($errors, Validate::checkWeight($product->getWeight()));
            $errors = array_merge($errors, Validate::checkLength($product->getLength()));
            $errors = array_merge($errors, Validate::checkWidth($product->getWidth()));
            $errors = array_merge($errors, Validate::checkHeight($product->getHeight()));
            $errors = array_merge($errors, Validate::checkCount($product->getCount()));

            /** @var \Entities\Property[] $properties */
            $properties = Property::getPropertiesByCategoryID($product->getCategoryID());

            foreach ($_POST as $postK => $postV) {
                if (strpos($postK, 'property_') === 0) {

                    foreach ($properties as $property) {
                        if (str_replace('property_', '', $postK) == $property->getName()) {

                            $propertyAndPropertyValues[$property->getID()]['property'] = $property;

                            switch ($property->getType()) {
                                case 'string':
                                    $propertyAndPropertyValues[$property->getID()]['property_value'] = new \Entities\PropertyValue('', $postV);
                                    break;

                                case 'integer':
                                    $errorText = 'Свойство "' . $property->getLabel() . '" должно быть числом';
                                    $errors = array_merge($errors, Validate::checkPropertyTypeInteger($property->getName(), $postV, $errorText));
                                    $propertyAndPropertyValues[$property->getID()]['property_value'] = new \Entities\PropertyValue('', $postV);
                                    break;

                                case 'boolean':

                                    if ($postV == '1') {
                                        $postV = '1';
                                    } else {
                                        $postV = '0';
                                    }

                                    $propertyAndPropertyValues[$property->getID()]['property_value'] = new \Entities\PropertyValue('', $postV);
                                    break;
                            }
                        }
                    }
                }
            }

            if (!count($errors)) {
                if (Product::create($product, $propertyAndPropertyValues)) {
                    header('Location: /admin/product/');
                } else {
                    $errors['create'] = 'Продукт не был добавлен';
                }
            }
        }
        require_once ROOT . '/views/admin/product/create.php';
        return true;
    }

    public function actionRead()
    {
        self::checkAdmin();

        $errors = [];
        $fullProducts = Product::getProductsByCategories();

        if (!count($fullProducts)) {
            $errors['read'] = 'Нет продуктов';
        }
        require_once ROOT . '/views/admin/product/read.php';
        return true;
    }

    public function actionUpdate($id)
    {
        self::checkAdmin();

        if (intval($id)) {
            $errors = [];
            $categories = Category::getCategories();
            $properties = [];
            $propertyValues = [];
            $propertyAndPropertyValues = [];
            $relationshipPropertiesAndPropertyValues = [];

            /** @var Entities\Product $product */
            $product = Product::getProductByID($id);
            if ($product) {

                /** @var \Entities\RelationshipPropertyAndPropertyValue $relationshipPropertiesAndPropertyValues */
                $relationshipPropertiesAndPropertyValues = RelationshipPropertyAndPropertyValue::getRelationshipPropertiesAndPropertyValuesByProduct($product);

                /** @var \Entities\Property[] $properties */
                $properties = Property::getPropertiesByCategoryID($product->getCategoryID());

                /** @var \Entities\PropertyValue $propertyValues[] */
                $propertyValues = PropertyValue::getPropertyValuesByPropertiesAndProduct($properties, $product);

                if ($_POST['update_product_submit']) {
                    $product->setID(intval($id));
                    $product->setLabel($_POST['label']);
                    $product->setName($_POST['name']);
                    $product->setSort($_POST['sort']);
                    $product->setActive($_POST['active'] == 'on' ? 1 : 0);
                    $product->setCategoryID(intval($_POST['category_id']) === 0 ? null : intval($_POST['category_id']));
                    $product->setBrand($_POST['brand']);
                    $product->setPrice($_POST['price']);
                    $product->setWeight($_POST['weight']);
                    $product->setLength($_POST['length']);
                    $product->setWidth($_POST['width']);
                    $product->setHeight($_POST['height']);
                    $product->setShortDescription($_POST['short_description']);
                    $product->setFullDescription($_POST['full_description']);
                    $product->setCount($_POST['count']);

                    /** @var \Entities\Property[] $properties */
                    $properties = Property::getPropertiesByCategoryID($product->getCategoryID());

                    foreach ($_POST as $postK => $postV) {
                        if (strpos($postK, 'property_') === 0) {

                            foreach ($properties as $property) {
                                if (str_replace('property_', '', $postK) == $property->getName()) {

                                    $propertyAndPropertyValues[$property->getID()]['property'] = $property;

                                    switch ($property->getType()) {
                                        case 'string':
                                            $propertyAndPropertyValues[$property->getID()]['property_value'] = new \Entities\PropertyValue('', $postV);
                                            break;

                                        case 'integer':
                                            $errorText = 'Свойство "' . $property->getLabel() . '" должно быть числом';
                                            $errors = array_merge($errors, Validate::checkPropertyTypeInteger($property->getName(), $postV, $errorText));
                                            $propertyAndPropertyValues[$property->getID()]['property_value'] = new \Entities\PropertyValue('', $postV);
                                            break;

                                        case 'boolean':

                                            if ($postV == '1') {
                                                $postV = '1';
                                            } else {
                                                $postV = '0';
                                            }

                                            $propertyAndPropertyValues[$property->getID()]['property_value'] = new \Entities\PropertyValue('', $postV);
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    $errors = array_merge($errors, Validate::checkLabel($product->getLabel()));
                    $errors = array_merge($errors, Validate::checkName($product->getName()));
                    $errors = array_merge($errors, Validate::checkSort($product->getSort()));
                    $errors = array_merge($errors, Validate::checkCategoryID($product->getCategoryID()));
                    $errors = array_merge($errors, Validate::checkPrice($product->getPrice()));
                    $errors = array_merge($errors, Validate::checkWeight($product->getWeight()));
                    $errors = array_merge($errors, Validate::checkLength($product->getLength()));
                    $errors = array_merge($errors, Validate::checkWidth($product->getWidth()));
                    $errors = array_merge($errors, Validate::checkHeight($product->getHeight()));
                    $errors = array_merge($errors, Validate::checkCount($product->getCount()));

                    if (!count($errors)) {
                        if (Product::update($product, $propertyAndPropertyValues)) {
                            header('Location: /admin/product/');
                        } else {
                            $errors['update'] = 'Продукт не был изменен';
                        }
                    }
                }
            } else {
                $errors['update'] = 'Продукт не найден';
            }
            require_once ROOT . '/views/admin/product/update.php';
            return true;
        }
        return false;
    }

    public function actionDelete($id)
    {
        self::checkAdmin();

        if (intval($id)) {
            $errors = [];
            $product = Product::getProductByID($id);
            if ($product) {
                if ($_POST['delete_product_submit']) {
                    if (Product::delete($id)) {
                        header('Location: /admin/product/');
                    } else {
                        $errors['delete'] = 'Продукт не был удален';
                    }
                }
            } else {
                $errors['delete'] = 'Продукт не был найден';
            }
            require_once ROOT . '/views/admin/product/delete.php';
            return true;
        }
        return false;
    }
}
