<?php

use Models\Category;
use Components\Validate;
use Components\Admin;

class AdminCategoryController extends Admin
{
    public function actionCreate()
    {
        self::checkAdmin();

        $errors = [];
        $category = new Entities\Category();
        if ($_POST['create_category_submit']) {
            $category->setLabel($_POST['label']);
            $category->setName($_POST['name']);
            $category->setSort($_POST['sort']);
            $category->setActive($_POST['active'] == 'on' ? 1 : 0);
            $errors = array_merge($errors, Validate::checkLabel($category->getLabel()));
            $errors = array_merge($errors, Validate::checkName($category->getName()));
            $errors = array_merge($errors, Validate::checkSort($category->getSort()));
            if (!count($errors)) {
                if (Category::create($category)) {
                    header('Location: /admin/category/');
                } else {
                    $errors['create'] = 'Категория не была добавлена';
                }
            }
        }
        require_once ROOT . '/views/admin/category/create.php';
        return true;
    }

    public function actionRead()
    {
        self::checkAdmin();

        $errors = [];
        $categories = Category::getCategories();
        if (!count($categories)) {
            $errors['read'] = 'Нет категорий';
        }
        require_once ROOT . '/views/admin/category/read.php';
        return true;
    }

    public function actionUpdate($id)
    {
        self::checkAdmin();

        if (intval($id)) {
            $errors = [];
            /** @var Entities\Category $category */
            $category = Category::getCategoryByID($id);
            if ($category) {
                if ($_POST['update_category_submit']) {
                    $category->setID(intval($id));
                    $category->setLabel($_POST['label']);
                    $category->setName($_POST['name']);
                    $category->setSort($_POST['sort']);
                    $category->setActive($_POST['active'] == 'on' ? 1 : 0);
                    $errors = array_merge($errors, Validate::checkLabel($category->getLabel()));
                    $errors = array_merge($errors, Validate::checkName($category->getName()));
                    $errors = array_merge($errors, Validate::checkSort($category->getSort()));
                    if (!count($errors)) {
                        if (Category::update($category)) {
                            header('Location: /admin/category/');
                        } else {
                            $errors['update'] = 'Категория не была изменена';
                        }
                    }
                }
            } else {
                $errors['update'] = 'Категория не найдена';
            }
            require_once ROOT . '/views/admin/category/update.php';
            return true;
        }
        return false;
    }

    public function actionDelete($id)
    {
        self::checkAdmin();

        if (intval($id)) {
            $errors = [];
            $category = Category::getCategoryByID($id);
            if ($category) {
                if ($_POST['delete_category_submit']) {
                    if (Category::delete($id)) {
                        header('Location: /admin/category/');
                    } else {
                        $errors['delete'] = 'Категория не была удалена';
                    }
                }
            } else {
                $errors['delete'] = 'Категория не найдена';
            }
            require_once ROOT . '/views/admin/category/delete.php';
            return true;
        }
        return false;
    }
}
