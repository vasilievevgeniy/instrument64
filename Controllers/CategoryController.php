<?php

use Models\Category;
use Models\Product;
use Models\Property;

class CategoryController
{
    public function actionDetails($id)
    {
        $categories = Category::getCategories();
        $products = Product::getProductsByCategoryID($id);
        $currentCategory = Category::getCategoryByID($id);

        $brands = Product::getBrands($id);
        $maxWeight = Product::getMaxWeight($id);
        $minWeight = Product::getMinWeight($id);
        $maxLength = Product::getMaxLength($id);
        $minLength = Product::getMinLength($id);

        $maxWidth = Product::getMaxWidth($id);
        $minWidth = Product::getMinWidth($id);
        $maxHeight = Product::getMaxHeight($id);
        $minHeight = Product::getMinHeight($id);

        $maxPrice = Product::getMaxPrice($id);
        $minPrice = Product::getMinPrice($id);

        if ($_POST) {
            $products = Product::getProductsByFilter($_POST, $id);
        }

        $propertiesByCategory = Property::getPropertiesByCategoryID($id);

        require_once ROOT . '/views/category/details.php';

        return true;
    }
}
