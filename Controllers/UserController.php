<?php

use Models\User;
use Models\Category;
use Models\Order;

/**
 * Контроллер UserController
 */
class UserController
{
    /**
     * Action для страницы "Регистрация"
     */
    public function actionRegister()
    {
        // Переменные для формы
        $name = false;
        $email = false;
        $phone = false;
        $password = false;
        $result = false;

        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена 
            // Получаем данные из формы
            $name = $_POST['name'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $password = $_POST['password'];

            // Флаг ошибок
            $errors = false;

            // Валидация полей
            if (!User::checkName($name)) {
                $errors[] = 'Имя не должно быть короче 2-х символов';
            }
            if (!User::checkEmail($email)) {
                $errors[] = 'Неправильный email';
            }
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }
            if (User::checkEmailExists($email)) {
                $errors[] = 'Такой email уже используется';
            }
            
            if ($errors == false) {
                // Если ошибок нет
                // Регистрируем пользователя
                $result = User::register($name, $email, $phone, $password);
            }
        }

        $categories = Category::getCategories();

        // Подключаем вид
        require_once(ROOT . '/views/user/register.php');
        return true;
    }
    
    /**
     * Action для страницы "Вход на сайт"
     */
    public function actionLogin()
    {
        // Переменные для формы
        $email = false;
        $password = false;
        
        // Обработка формы
        if (isset($_POST['submit'])) {
            // Если форма отправлена 
            // Получаем данные из формы
            $email = $_POST['email'];
            $password = $_POST['password'];

            // Флаг ошибок
            $errors = false;

            // Валидация полей
            if (!User::checkEmail($email)) {
                $errors[] = 'Неправильный email';
            }
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }

            // Проверяем существует ли пользователь
            $userId = User::checkUserData($email, $password);

            if ($userId == false) {
                // Если данные неправильные - показываем ошибку
                $errors[] = 'Неправильные данные для входа на сайт';
            } else {
                // Если данные правильные, запоминаем пользователя (сессия)
                User::auth($userId);

                // Перенаправляем пользователя в закрытую часть - кабинет
                $user = User::getUserById($userId);
                if ($user['role'] == 'admin') {
                    header("Location: /admin/");
                }
                header("Location: /");
            }
        }

        $categories = Category::getCategories();

        // Подключаем вид
        require_once(ROOT . '/views/user/login.php');
        return true;
    }

    /**
     * Удаляем данные о пользователе из сессии
     */
    public function actionLogout()
    {
        // Стартуем сессию
        session_start();
        
        // Удаляем информацию о пользователе из сессии
        unset($_SESSION["user"]);
        
        // Перенаправляем пользователя на главную страницу
        header("Location: /");
    }

    public function actionOrder()
    {
        $user = User::getUserBySession();

        if (!$user) {
            header('Location: /');
        }

        // Получаем список заказов
        $ordersList = Order::getOrdersList();

        $ordersList = Order::getOrdersLIstByUserID($user['id']);

        $categories = Category::getCategories();

        // Подключаем вид
        require_once(ROOT . '/views/user/order.php');
        return true;
    }

    public function actionOrderView($id)
    {
        if ($id) {

            $user = User::getUserBySession();

            if (!$user) {
                header('Location: /');
            }

            $categories = Category::getCategories();

            // Получаем данные о конкретном заказе
            $order = Order::getOrderById($id);

            // Получаем массив с идентификаторами и количеством товаров
            $productsQuantity = json_decode($order['products'], true);

            // Получаем массив с индентификаторами товаров
            $productsIds = array_keys($productsQuantity);

            // Получаем список товаров в заказе
            $products = Models\Product::getProdustsByIds($productsIds);

            require_once(ROOT . '/views/user/order/view.php');
            return true;
        }
        return false;
    }

    public function actionAccount()
    {
        if ($user = User::getUserBySession()) {
            if (isset($_POST['submit'])) {
                $user['name'] = $_POST['name'];
                $user['email'] = $_POST['email'];
                $user['phone'] = $_POST['phone'];
                $user['password'] = $_POST['password'];
                $errors = false;
                if (!User::checkName($user['name'])) {
                    $errors[] = 'Имя не должно быть короче 2-х символов';
                }
                if (!User::checkEmail($user['email'])) {
                    $errors[] = 'Неправильный email';
                }
                if (!User::checkPassword($user['password'])) {
                    $errors[] = 'Пароль не должен быть короче 6-ти символов';
                }
                if (User::checkEmailExists($user['password'])) {
                    $errors[] = 'Такой email уже используется';
                }
                if ($errors == false) {
                    $result = User::update($user['id'], $user['name'], $user['email'], $user['phone'], $user['password']);
                }
            }
            $categories = Category::getCategories();
            require_once ROOT . '/views/user/update.php';
        }
        return true;
    }
}
