<?php

use Models\Category;
use Models\Property;
use Models\Product;
use Components\Admin;

class AdminController extends Admin
{
    public function actionMain()
    {
        self::checkAdmin();

        $errors = [];
        $categories = Category::getCategories();
        $properties = Property::getProperties();
        $products = Product::getProducts();
        if (!count($categories)) {
            $errors['categories'] = 'Нет категорий';
        }
        if (!count($properties)) {
            $errors['properties'] = 'Нет свойств';
        }
        if (!count($products)) {
            $errors['products'] = 'Нет продуктов';
        }
        require_once ROOT . '/views/admin/main.php';
        return true;
    }
}