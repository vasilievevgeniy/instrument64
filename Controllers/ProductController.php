<?php

use Models\Category;
use Models\Product;
use Models\Property;
use Models\PropertyValue;
use Models\RelationshipPropertyAndPropertyValue;

class ProductController
{
    public function actionDetails($id)
    {
        $categories = Category::getCategories();
        $product = Product::getProductByID($id);

        /** @var \Entities\RelationshipPropertyAndPropertyValue $relationshipPropertiesAndPropertyValues */
        $relationshipPropertiesAndPropertyValues = RelationshipPropertyAndPropertyValue::getRelationshipPropertiesAndPropertyValuesByProduct($product);
        /** @var \Entities\Property[] $properties */
        $properties = Property::getPropertiesByCategoryID($product->getCategoryID());
        /** @var \Entities\PropertyValue $propertyValues[] */
        $propertyValues = PropertyValue::getPropertyValuesByPropertiesAndProduct($properties, $product);

        require_once ROOT . '/views/product/details.php';

        return true;
    }
}