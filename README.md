# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
    * Apache-PHP-7
    * PHP-7
* Dependencies
* Database configuration
    * MySQL-5.7-x64
* How to run tests
* Deployment instructions
    * Администратор должен дать права разработчику на Bitbucket: Репозиторий -> Настройки -> Юзер и группы -> Найти разработчика по почте и дать права.
    * Установить Open Server.
    * Установить Git Bash. С его помощью в папку openserver/domain склонировать репозиторий проекта командой: git clone https://bitbucket.org/vasilievevgeniy/instrument64.git.
    * В проекте из папки /deploy/ скопировать файл area.php в папку /config/. В этом файле раскомментировать настройку окружения.
    * В Open Server добавить домен instrument64, который должен ссылаться на папку проекта /openserver/domains/instrument64/.
    * В Git Bash в папке с проектом настроить для Git имя и e-mail.
    * Создать БД instrument64 (host: localhost, user: root, pass пустой).
    * Выполнить команду composer install в корне сайта.
    * Настройка phinx. Скопировать файл phinx.yml из папки /deploy/ в папку /vendor/bin/. Установить в файле БД по умолчанию.
    * Выполнить миграции из папки /vendor/bin/ командой phinx migrate.
    * Установить node.js. Скачать пакеты npm при помощи комманды: npm install npm@latest -g.
    * Перезагрузить Open Server.
    * Создать в корне папку upload.
    * Создать пользователя в базе с role admin.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact