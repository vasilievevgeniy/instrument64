<?php


use Phinx\Migration\AbstractMigration;

class CreateTablesValueProperty extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('property_value');
        $table
            ->addColumn('value', 'string')
            ->addColumn('sort', 'integer')
            ->addColumn('active', 'enum', ['values' => [1, 0]])
            ->create();

        $table = $this->table('ppp');
        $table
            ->addColumn('product_id', 'integer', ['null' => true])
            ->addColumn('property_id', 'integer', ['null' => true])
            ->addColumn('property_value_id', 'integer', ['null' => true])
            ->addColumn('type', 'enum', ['values' => ['string', 'integer', 'boolean']])
            ->addForeignKey('product_id', 'product', 'id', ['delete'=> 'RESTRICT', 'update'=> 'RESTRICT'])
            ->addForeignKey('property_id', 'property', 'id', ['delete'=> 'RESTRICT', 'update'=> 'RESTRICT'])
            ->addForeignKey('property_value_id', 'property_value', 'id', ['delete'=> 'RESTRICT', 'update'=> 'RESTRICT'])
            ->create();
    }
}
