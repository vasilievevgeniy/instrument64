<?php

namespace Components;

class DB
{
    public static function getConnection()
    {
        define('ROOT', __DIR__ . '/../');

        $area = null;
        $areaPath = ROOT . '/config/area.php';
        if (file_exists($areaPath)) {
            $area = require $areaPath;
        }

        $db = [];
        $dbPath = ROOT . '/config/db.php';
        if (file_exists($dbPath)) {
            $db = require $dbPath;
        }

        $dbArea = [];
        if ($area && count($db)) {
            $dbArea = $db[$area];
        }

        if (count($dbArea)) {
            return new \PDO('mysql:host=' . $dbArea['host'] . ';dbname=' .
                $dbArea['dbname'], $dbArea['user'], $dbArea['pass']);
        }
        return false;
    }
}
