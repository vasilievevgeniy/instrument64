<?php

namespace Components;

class Router
{
    public function call()
    {
        $url = $this->getUrl();
        $routes = $this->getRoutes();
        $this->callController($url, $routes);
    }

    private function getUrl()
    {
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }

    private function getRoutes()
    {
        $pathRoutes = ROOT . '/config/routes.php';
        if (file_exists($pathRoutes)) {
            return require_once $pathRoutes;
        }
        return false;
    }

    private function callController($url, $routes)
    {
        if ($url && count($routes)) {
            foreach ($routes as $routeK => $routeV) {
                if (preg_match("`$routeK`", $url)) {
                    $controllerData = preg_replace("`$routeK`", $routeV, $url);
                    $controllerData = explode('/', $controllerData);
                    $controllerName = ucfirst(array_shift($controllerData)) . 'Controller';
                    $actionName = 'action' . ucfirst(array_shift($controllerData));
                    $parameters = $controllerData;
                    $pathController = ROOT . '/Controllers/' . $controllerName . '.php';
                    if (file_exists($pathController)) {
                        require_once $pathController;
                        $result = call_user_func_array([$controllerName, $actionName], $parameters);
                        if ($result) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
