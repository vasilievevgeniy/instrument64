<?php

namespace Components;

class Validate
{
    public static function checkLabel($label)
    {
        $errors = [];
        if (!$label) {
            $errors['label'] = 'Введите название';
        } else {
            if (!preg_match("/^[0-9A-Za-zА-Яа-я- ]+$/u", $label)) {
                $errors['label'] = 'Допутимые символы названия "/^[0-9A-Za-zА-Яа-я-_]+$/u"';
            }
        }
        return $errors;
    }

    public static function checkName($name)
    {
        $errors = [];
        if (!$name) {
            $errors['name'] = 'Введите код названия';
        } else {
            if (!preg_match("/^[0-9a-z-_]+$/", $name)) {
                $errors['name'] = 'Допутимые символы кода названия "/^[0-9a-z-_]+$/"';
            }
        }
        return $errors;
    }

    public static function checkSort($sort)
    {
        $errors = [];
        if ($sort) {
            if (!preg_match("/^[0-9]+$/", $sort)) {
                $errors['sort'] = 'Порядок должен быть числом';
            }
        }
        return $errors;
    }

    public static function checkType($type)
    {
        $errors = [];
        if (!$type) {
            $errors['type'] = 'Выберите тип';
        } else {
            if ($type !== 'integer' && $type !== 'string' && $type !== 'boolean') {
                $errors['type'] = 'Не верный формат типа';
            }
        }
        return $errors;
    }

    public static function checkCategoryID($categoryID)
    {
        $errors = [];
        if ($categoryID) {
            if (!preg_match("/^[0-9]+$/", $categoryID)) {
                $errors['category_id'] = 'Категория должна быть числом';
            }
        }
        return $errors;
    }

    public static function checkPrice($price)
    {
        $errors = [];
        if ($price) {
            if (!preg_match("/^[0-9]+$/", $price)) {
                $errors['price'] = 'Цена должна быть числом';
            }
        }
        return $errors;
    }

    public static function checkWeight($weight)
    {
        $errors = [];
        if ($weight) {
            if (!preg_match("/^[0-9]+$/", $weight)) {
                $errors['weight'] = 'Вес должнен быть числом';
            }
        }
        return $errors;
    }

    public static function checkLength($length)
    {
        $errors = [];
        if ($length) {
            if (!preg_match("/^[0-9]+$/", $length)) {
                $errors['length'] = 'Длина должна быть числом';
            }
        }
        return $errors;
    }

    public static function checkWidth($width)
    {
        $errors = [];
        if ($width) {
            if (!preg_match("/^[0-9]+$/", $width)) {
                $errors['width'] = 'Ширина должна быть числом';
            }
        }
        return $errors;
    }

    public static function checkHeight($height)
    {
        $errors = [];
        if ($height) {
            if (!preg_match("/^[0-9]+$/", $height)) {
                $errors['height'] = 'Высота должна быть числом';
            }
        }
        return $errors;
    }

    public static function checkCount($count)
    {
        $errors = [];
        if ($count) {
            if (!preg_match("/^[0-9]+$/", $count)) {
                $errors['count'] = 'Количество должно быть числом';
            }
        }
        return $errors;
    }

    public static function checkCategoryIDs($categoryIDs)
    {
        $errors = [];
        if (count($categoryIDs)) {
            foreach ($categoryIDs as $categoryID) {
                if ($categoryID) {
                    if (!preg_match("/^[0-9]+$/", $categoryID)) {
                        $errors['category_ids'] = 'Ошибка';
                    }
                }
            }
        }
        return $errors;
    }

    public static function checkPropertyTypeInteger($propertyName, $value, $errorText)
    {
        $errors = [];
        if ($propertyName && $value && $errorText) {
            if (!preg_match("/^[0-9]+$/", $value)) {
                $errors[$propertyName] = $errorText;
            }
        }
        return $errors;
    }
}
