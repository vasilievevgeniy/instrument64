<?php

return [
    '/admin/category/create/' => 'adminCategory/create',
    '/admin/category/update/([0-9]+)/' => 'adminCategory/update/$1',
    '/admin/category/delete/([0-9]+)/' => 'adminCategory/delete/$1',
    '/admin/category/' => 'adminCategory/read',

    '/admin/product/create/' => 'adminProduct/create',
    '/admin/product/update/([0-9]+)/' => 'adminProduct/update/$1',
    '/admin/product/delete/([0-9]+)/' => 'adminProduct/delete/$1',
    '/admin/product/' => 'adminProduct/read',

    '/admin/property/create/' => 'adminProperty/create',
    '/admin/property/update/([0-9]+)/' => 'adminProperty/update/$1',
    '/admin/property/delete/([0-9]+)/' => 'adminProperty/delete/$1',
    '/admin/property/' => 'adminProperty/read',

    // Пользователь:
    '/user/order/view/([0-9]+)/' => 'user/orderView/$1',
    '/user/register/' => 'user/register',
    '/user/login/' => 'user/login',
    '/user/logout/' => 'user/logout',
    '/user/order/' => 'user/order',
    '/user/account/' => 'user/account',

    // Корзина:
    '/cart/checkout/' => 'cart/checkout', // actionAdd в CartController
    '/cart/delete/([0-9]+)/' => 'cart/delete/$1', // actionDelete в CartController
    '/cart/add/([0-9]+)/' => 'cart/add/$1', // actionAdd в CartController
    '/cart/add_ajax/([0-9]+)/' => 'cart/addAjax/$1', // actionAddAjax в CartController
    '/cart/' => 'cart/index', // actionIndex в CartController

    // Управление заказами:
    '/admin/order/update/([0-9]+)/' => 'adminOrder/update/$1',
    '/admin/order/delete/([0-9]+)/' => 'adminOrder/delete/$1',
    '/admin/order/view/([0-9]+)/' => 'adminOrder/view/$1',
    '/admin/order/' => 'adminOrder/index',

    '/admin/' => 'admin/main',

    '/category/([0-9]+)/' => 'category/details/$1',

    '/product/([0-9]+)/' => 'product/details/$1',

    '/about/' => 'main/about',
    '/feedback/' => 'main/feedback',
    '/contacts/' => 'main/contacts',
    '/' => 'main/main',
];
