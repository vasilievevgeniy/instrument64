<?php

use Components\Router;

session_start();

define('ROOT', __DIR__);

require ROOT . '/vendor/autoload.php';

$router = new Router();
$router->call();