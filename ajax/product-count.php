<?php

use Models\Product;

session_start();

require __DIR__ . '/../vendor/autoload.php';

$productID = $_POST['productID'];
$productCount = $_POST['productCount'];

$product = Product::getProductByID($productID);

if ($product->getCount() < $productCount) {
    $productCount = $product->getCount();
}
$_SESSION['products'][$productID] = intval($productCount);

echo $productID . '/' . $productCount;
