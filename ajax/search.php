<?php

require __DIR__ . '/../vendor/autoload.php';

$products = Models\Product::getProductByLabelSearch($_POST['search']);

$productsArr = [];
/** @var Entities\Product $product */
foreach ($products as $product) {
    $productsArr['results'][] = [
        "title" => $product->getLabel(),
        "url" =>  "/product/" . $product->getID() . '/',
        "text" =>  ""
    ];
}

$productJson = json_encode($productsArr, true);

echo $productJson;