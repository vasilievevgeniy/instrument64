<?php

use Models\Property;

require __DIR__ . '/../vendor/autoload.php';

$properties = Property::getPropertiesByCategoryID($_POST['categoryID']);

if (count($properties)) {
    /** @var Entities\Property $property */
    foreach ($properties as $property) {
        switch ($property->getType()) {
            case 'string':
                ?>
                <div class="uk-form-row">
                    <label class="uk-form-label"><?=$property->getLabel()?></label>
                    <div class="uk-form-controls">
                        <input type="text" name="property_<?=$property->getName()?>" placeholder="<?=$property->getLabel()?>" value="">
                    </div>
                </div>
                <?
                break;
            case 'integer':
                ?>
                <div class="uk-form-row">
                    <label class="uk-form-label"><?=$property->getLabel()?></label>
                    <div class="uk-form-controls">
                        <input type="text" name="property_<?=$property->getName()?>" placeholder="<?=$property->getLabel()?>" value="">
                    </div>
                </div>
                <?
                break;
            case 'boolean':
                ?>
                <div class="uk-form-row">
                    <label class="uk-form-label"><?=$property->getLabel()?></label>
                    <div class="uk-form-controls">
                        <div>Да <input type="radio" name="property_<?=$property->getName()?>" value="1"></label></div>
                        <div>Нет <input checked type="radio" name="property_<?=$property->getName()?>" value="0"></label></div>
                    </div>
                </div>
                <?
                break;
        }
    }
}