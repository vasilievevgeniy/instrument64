$(document).ready(function() {
    $('select[name=category_id]').change(function() {
        var categoryID = $(this).val();
        if (categoryID) {
            $.ajax({
                type: 'post',
                url: '/ajax/getPropertiesByCategoryID.php',
                data: {
                    categoryID: categoryID
                },
                success: function(data){
                    $('.properties').html(data);
                }
            });
        }
    });

    $(".add-to-cart").click(function () {
        var id = $(this).attr("data-id");
        $.post("/cart/add_ajax/"+id+"/", {}, function (data) {
            $("#cart-count").html(data);
            UIkit.notify("Товар добавлен в корзину");
        });
        return false;
    });

});