</div>
<div class="tm-footer uk-margin-large-top uk-margin-large-bottom">
    <div class="uk-container uk-container-center uk-text-center">
        <ul class="uk-subnav uk-subnav-line uk-flex-center">
            <li><a href="/">Главная</a></li>
            <li><a href="/about/">О нас</a></li>
            <li><a href="/feedback/">Обратная связь</a></li>
            <li><a href="/contacts/">Контакты</a></li>
        </ul>
        <div class="uk-panel">
            <p>Мы занимаемся инструментом</p>
            <p><i class="uk-icon-phone"></i> +7 (987) 358-41-92</p>
            <p><i class="uk-icon-map-o"></i> г. Саратов, ул. Огородная, д. 158а</p>
            <a href="/"><img src="/templates/images/logo.png" width="70" height="30"><div>© 2018 Instrument64</div></a>
        </div>
    </div>
</div>
</body>
</html>