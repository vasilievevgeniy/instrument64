<?

/** @var \Entities\Product[] $products */

if(count($products)):?>
    <?foreach($products as $product):?>
        <?if($product->getActive()):?>
            <div class="uk-margin-bottom uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-1">
                <div class="uk-panel-box">
                    <?if($product->getImage()):?>
                        <a href="/upload/<?=$product->getImage()?>" data-uk-lightbox title="">
                            <img width="400" class="uk-float-left uk-margin-right uk-margin-bottom" src="/upload/<?=$product->getImage()?>">
                        </a>
                    <?endif;?>
                    <?if($product->getName()):?>
                        <h4 class="uk-panel-title"><?=$product->getLabel()?></h4>
                    <?endif;?>
                    <?if($product->getBrand()):?>
                        <div>Бренд: <?=$product->getBrand()?></div>
                    <?endif;?>
                    <?if($product->getWeight()):?>
                        <div>Вес: <?=$product->getWeight()?> гр.</div>
                    <?endif;?>
                    <?if($product->getLength()):?>
                        <div>Длина: <?=$product->getLength()?> мм.</div>
                    <?endif;?>
                    <?if($product->getWidth()):?>
                        <div>Ширина: <?=$product->getWidth()?> мм.</div>
                    <?endif;?>
                    <?if($product->getHeight()):?>
                        <div>Высота: <?=$product->getHeight()?> мм.</div>
                    <?endif;?>
                    <?if($product->getCount()):?>
                        <div>Количество: <span class="product_count_<?=$product->getID()?>"><?=$product->getCount()?></span> шт.</div>
                    <?else:?>
                        <div class="product_count"><span class="product_count_<?=$product->getID()?>">Нет в наличии</span></div>
                    <?endif;?>
                    <?if($product->getShortDescription()):?>
                        <p><?=$product->getShortDescription()?></p>
                    <?endif;?>
                    <div class="uk-grid uk-grid-collapse">
                        <div class="uk-width-3-5">
                            <a class="uk-button uk-button-primary uk-margin-small-bottom" href="/product/<?=$product->getID()?>/"><i class="uk-icon-eye"></i> Посмотреть</a>
                            <?if($product->getCount()):?>
                                <a href="" class="add-to-cart uk-button uk-button-success" data-id="<?=$product->getID()?>"><i class="uk-icon-cart-plus"></i> В корзину</a>
                            <?endif;?>
                        </div>
                        <div class="uk-width-2-5" style="text-align: center;">
                            <?if($product->getPrice()):?>
                                <div class="uk-margin-small-top price-font"><?=number_format($product->getPrice(), 0, '', ' ')?> <i class="uk-icon-rub"></i></div>
                            <?endif;?>
                        </div>
                    </div>
                </div>
            </div>
        <?endif;?>
    <?endforeach;?>
<?endif;?>