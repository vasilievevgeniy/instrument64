<html>
<head>
    <meta charset="utf-8">
    <title>Инструмент-64</title>
    <link rel="shortcut icon" href="/templates/images/instrument64.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/templates/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/uikit.gradient.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/slideshow.gradient.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/slidenav.gradient.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/dotnav.gradient.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/search.gradient.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/autocomplete.gradient.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/uikit/dist/css/components/notify.gradient.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/jquery-ui-npm/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/node_modules/chosen-js/chosen.min.css">
    <script src="/node_modules/uikit/vendor/jquery.js"></script>
    <script src="/node_modules/uikit/dist/js/uikit.js"></script>
    <script src="/node_modules/uikit/dist/js/components/slideshow.js"></script>
    <script src="/node_modules/uikit/dist/js/components/lightbox.js"></script>
    <script src="/node_modules/uikit/dist/js/components/search.js"></script>
    <script src="/node_modules/uikit/dist/js/components/autocomplete.js"></script>
    <script src="/node_modules/uikit/dist/js/components/notify.js"></script>
    <script src="/node_modules/jquery-ui-npm/jquery-ui.js"></script>
    <script src="/node_modules/chosen-js/chosen.jquery.min.js"></script>
    <script src="/node_modules/jquery.maskedinput/src/jquery.maskedinput.js"></script>
    <script src="/templates/js/scripts.js"></script>
</head>
<body>
<div class="uk-container uk-container-center uk-margin-top">
    <nav class="uk-navbar uk-margin-bottom">
        <ul class="uk-navbar-nav">
            <li><a href="/"><i class="uk-icon-home"></i> Главная</a></li>
            <li><a href="/about/">О нас</a></li>
            <li><a href="/feedback/">Обратная связь</a></li>
            <li><a href="/contacts/">Контакты</a></li>
        </ul>
        <div class="uk-navbar-flip">
            <ul class="uk-navbar-nav">

                <li>
                    <form class="uk-search uk-margin-small-top" data-uk-search="{source:'http://instrument64/ajax/search.php'}">
                        <input class="uk-search-field" type="search" placeholder="Поиск">
                        <div class="uk-dropdown uk-dropdown-search">
                            <ul class="uk-nav uk-nav-search">...</ul>
                        </div>
                    </form>
                </li>

                <li><a href="/cart/">
                        <i class="uk-icon-shopping-cart"></i> Корзина
                        (<span id="cart-count"><?php echo \Components\Cart::countItems(); ?></span>)
                    </a>
                </li>

                <?if(Models\User::isGuest()):?>
                    <li><a href="/user/login/"><i class="uk-icon-unlock-alt"></i> Войти</a></li>
                    <li><a href="/user/register/"><i class="uk-icon-user"></i> Регистрация</a></li>
                <?else:?>

                    <?if($currentUser = Models\User::getUserBySession()):?>
                        <li><a href="/user/account/">Личный кабинет</a></li>
                    <?endif;?>

                    <?if(Models\User::isAdmin()):?>
                        <li><a href="/admin/"><i class="uk-icon-user"></i> Администратор</a></li>
                    <?endif;?>
                    <li><a href="/user/logout/"><i class="uk-icon-lock"></i> Выход</a></li>
                <?endif;?>
            </ul>
        </div>
    </nav>