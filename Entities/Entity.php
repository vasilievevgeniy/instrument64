<?php

namespace Entities;

class Entity
{
    /** @var int $id */
    private $id;
    /** @var string $label */
    private $label;
    /** @var string $name */
    private $name;
    /** @var int $sort */
    private $sort;
    /** @var int $active */
    private $active;

    /**
     * Category constructor.
     * @param int $id
     * @param string $label
     * @param string $name
     * @param int $sort
     * @param int $active
     */
    public function __construct($id = 0, $label = '', $name = '', $sort = 0, $active = 0)
    {
        $this->id = $id;
        $this->label = $label;
        $this->name = $name;
        $this->sort = $sort;
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }
}