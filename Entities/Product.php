<?php

namespace Entities;

class Product extends Entity
{
    /** @var int $categoryID */
    private $categoryID;
    /** @var string $brand */
    private $brand;
    /** @var int $price */
    private $price;
    /** @var int $weight */
    private $weight;
    /** @var int $length */
    private $length;
    /** @var int $width */
    private $width;
    /** @var int $height */
    private $height;
    /** @var array $image */
    private $image;
    /** @var string $shortDescription */
    private $shortDescription;
    /** @var string $fullDescription */
    private $fullDescription;
    /** @var  integer $count */
    private $count;

    /**
     * Product constructor.
     * @param int $id
     * @param string $label
     * @param string $name
     * @param int $sort
     * @param int $active
     * @param int $categoryID
     * @param string $brand
     * @param int $price
     * @param int $weight
     * @param int $length
     * @param int $width
     * @param int $height
     * @param array $image
     * @param string $shortDescription
     * @param string $fullDescription
     * @param int $count
     */
    public function __construct(
        $id = 0,
        $label = '',
        $name = '',
        $sort = 0,
        $active = 0,
        $categoryID = null,
        $brand = '',
        $price = 0,
        $weight = 0,
        $length = 0,
        $width = 0,
        $height = 0,
        $image = [],
        $shortDescription = '',
        $fullDescription = '',
        $count = 0
    ) {
        parent::__construct($id, $label, $name, $sort, $active);
        $this->categoryID = $categoryID;
        $this->brand = $brand;
        $this->price = $price;
        $this->weight = $weight;
        $this->length = $length;
        $this->width = $width;
        $this->height = $height;
        $this->image = $image;
        $this->shortDescription = $shortDescription;
        $this->fullDescription = $fullDescription;
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getCategoryID()
    {
        return $this->categoryID;
    }

    /**
     * @param int $categoryID
     */
    public function setCategoryID($categoryID)
    {
        $this->categoryID = $categoryID;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param int $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return array
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return string
     */
    public function getFullDescription()
    {
        return $this->fullDescription;
    }

    /**
     * @param $fullDescription
     */
    public function setFullDescription($fullDescription)
    {
        $this->fullDescription = $fullDescription;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }
}