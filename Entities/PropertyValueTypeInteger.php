<?php

namespace Entities;

class PropertyValueTypeInteger extends PropertyValue
{
    public function __construct($id = 0, $value = 0, $sort = 0, $active = 0)
    {
        parent::__construct($id, $value, $sort, $active);
    }
}