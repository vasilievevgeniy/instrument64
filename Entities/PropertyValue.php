<?php

namespace Entities;

class PropertyValue
{
    private $id;
    private $value;
    private $sort;
    private $active;

    /**
     * PropertyValue constructor.
     * @param int $id
     * @param string $value
     * @param int $sort
     * @param int $active
     */
    public function __construct($id = 0, $value = '', $sort = 0, $active = 0)
    {
        $this->id = $id;
        $this->value = $value;
        $this->sort = $sort;
        $this->active = $active;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = $active;
    }
}