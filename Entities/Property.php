<?php

namespace Entities;

class Property extends Entity
{
    /** @var string $type */
    private $type;
    /** @var  string $unit */
    private $unit;

    /**
     * Property constructor.
     * @param int $id
     * @param string $label
     * @param string $name
     * @param int $sort
     * @param int $active
     * @param string $type
     * @param string $unit
     */
    public function __construct($id = 0, $label = '', $name = '', $sort = 0, $active = 0, $type = null, $unit ='')
    {
        parent::__construct($id, $label, $name, $sort, $active);
        $this->type = $type;
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }
}
