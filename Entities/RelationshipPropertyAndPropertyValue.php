<?php

namespace Entities;

class RelationshipPropertyAndPropertyValue
{
    private $id;
    private $productID;
    private $propertyID;
    private $propertyValueID;
    private $type;

    public function __construct($id = 0, $productID = 0, $propertyID = 0, $propertyValueID = 0, $type = null)
    {
        $this->id = $id;
        $this->productID = $productID;
        $this->propertyID = $propertyID;
        $this->propertyValueID = $propertyValueID;
        $this->type = $type;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getProductID()
    {
        return $this->productID;
    }

    public function setProductID($productID)
    {
        $this->productID = $productID;
    }

    public function getPropertyID()
    {
        return $this->propertyID;
    }

    public function setPropertyID($propertyID)
    {
        $this->propertyID = $propertyID;
    }

    public function getPropertyValueID()
    {
        return $this->propertyValueID;
    }

    public function setPropertyValueID($propertyValue)
    {
        $this->propertyValueID = $propertyValue;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }
}